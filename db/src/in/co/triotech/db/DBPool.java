package in.co.triotech.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;

import in.co.triotech.model.PropToTransaction;


public class DBPool {
    private static DataSource ds;

    static {
        DriverAdapterCPDS cpds = new DriverAdapterCPDS();
        try {
            cpds.setDriver(PropToTransaction.getDriverName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  
        }
        cpds.setUrl(PropToTransaction.getURL());
        cpds.setUser(PropToTransaction.getUserName());
        cpds.setPassword(PropToTransaction.getPassword());
System.out.println("Database Initiate");
        SharedPoolDataSource tds = new SharedPoolDataSource();
        tds.setConnectionPoolDataSource(cpds);
        tds.setMaxTotal(PropToTransaction.getDbPool());
        tds.setDefaultMaxIdle(PropToTransaction.getDbPool()/2);
        ds = tds;
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}