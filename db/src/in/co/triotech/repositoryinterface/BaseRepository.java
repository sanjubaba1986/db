package in.co.triotech.repositoryinterface;


public interface BaseRepository<TEntity> {
	
	public Integer add(TEntity model);

	public void update(TEntity model);

	public void remove(TEntity model);

	public void remove(Integer id);

	public TEntity get(Integer id);
	
	public TEntity merge(TEntity model);

}
