package in.co.triotech.triomoney.dao;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import in.co.triotech.connectivity.PropertyToMap;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Accounts;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Transdetail;


public class TransdetailsDAO {
	BasicTransCheck basicTransCheck =null;
	PropertyToMap propertyToMap = new PropertyToMap();
	Logger accessLog = Logger.getLogger("WL");
	public int getTransDeatils(Transdetail transdetail, BlockingQueue<Transaction> queue, Trans trans, Transaction transaction,Entity newEntity, Accounts newAccount,PropToTransaction propToTransaction)
	{
		int isCorrect = 0;String transMsg="";
		basicTransCheck =new BasicTransCheck();
		if (basicTransCheck.isMPincodeCorrect(transaction,transdetail.getMpin().toString(),propToTransaction)) {
			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", accessLog,"info",transaction.getTransId()+" ~ M pin is correct. initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
//			ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "INFO", "workerlogger", "M pin is correct  "+transaction.getInitiator()
//					+ transaction.getData() + " performed by "
//					+ Thread.currentThread().getName(),LogConfiguration.writeLogPath()+"Worker_Logs/"+transaction.getTo()+"/"+GetTimeStamp.getDate()+".log");
		   
			transdetail = basicTransCheck.getTransDetails(transaction.getInitiator(),transdetail.getTid().toString(),transaction,propToTransaction);
			if(transdetail!=null){
				transMsg=transdetail.getType()+":"+transdetail.getStatus()+" , TransID:"+trans.getId();
			}else{
				transMsg="Transaction not available.,TransID:"+trans.getId();
	
			}
			if(transaction.getOut()!=null){
				try {
					transaction.getOut().writeObject(transMsg);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName()+transaction.getThreadName());

				}
				try {
					transaction.getOut().flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
				try {
					transaction.getOut().close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
				}
				else{
					try {
						transaction.getOut().close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

					}
				}
			trans.setResultDesc(transMsg);
			trans.setState("2");
			try {
				new TransactionDaoService().updateCashinTrans(trans,transaction,propToTransaction);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
			isCorrect = 1;
		} else {
			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", accessLog,"info",transaction.getTransId()+" ~I: M pin is in-correct. initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
//			ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "INFO", "workerlogger", "M pin is in-correct  "+transaction.getInitiator()
//					+ transaction.getData() + " performed by "
//					+ Thread.currentThread().getName(),LogConfiguration.writeLogPath()+"Worker_Logs/"+transaction.getTo()+"/"+GetTimeStamp.getDate()+".log");
			
			try{
				if(transaction.getOut()!=null){
					transaction.getOut().writeObject("M-Pin is not correct."+",Transaction ID: "+trans.getId());
					transaction.getOut().flush();
					transaction.getOut().close();
					}
					else{
						transaction.getOut().close();
					}
				trans.setResultDesc(prepareMSG(propToTransaction,String.valueOf(trans.getId()),"msg.initiator.535","M-Pin is not correct"));
				trans.setState("3");
				new TransactionDaoService().updateCashinTrans(trans,transaction,propToTransaction);
				transaction=null;
				return isCorrect;
			}catch(Exception ex){StringWriter errors = new StringWriter();
				ex.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}

		}
		
		return isCorrect;
	}
	
	public boolean isInteger(String str) {
		int size = str.length();

		for (int i = 0; i < size; i++) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}

		return size > 0;
	}
	
	public String prepareMSG(PropToTransaction propToTransaction,String transID,String key,String defMessage)
	{
		String strMSG = propertyToMap.getPropertyValue(propToTransaction.getPropToMap(), key);
		if(strMSG != null)
		{
			strMSG = strMSG+" Transaction ID: "+transID;
		}
		else
		{
			strMSG = defMessage+",Transaction ID: "+transID;
		}
		
		return strMSG;
	}
}
