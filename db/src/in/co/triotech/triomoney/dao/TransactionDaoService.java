package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.TransExtDetails;
import in.co.triotech.triomoney.model.TransactionData;

public class TransactionDaoService {
	private TransactionDAO transactionDAO = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	Logger accessLog = Logger.getLogger("QL" + TransactionDaoService.class);

	public long save(Trans trans, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		long id = 0;
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			// conn = new ConnectionPool().getConnection();
			transactionDAO = new TransactionDAOImpl();

			id = transactionDAO.save(conn,
					"insert into Transaction(initiator,transTypeID,lastModified,debtor,creditor,amount,state,resultID,parentID,channelID,isAtomic) values(?,?,?,?,?,?,?,?,?,?,?)",
					new Object[] { trans.getInitiator(), trans.getTransTypeID(), trans.getLastModified(),
							trans.getDebtor(), trans.getCreditor(), trans.getAmount(), trans.getState(),
							trans.getResultID(), trans.getParentID(), trans.getChannelID(), trans.getIsAtomic() },
					transaction, propToTransaction);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName()
							+ transaction.getThreadName());

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return id;
	}

	public long saveCashinTrans(Transaction transactionData, String[] command, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		long id = 0;
		Trans trans = new Trans();
		trans.setInitiator(transactionData.getInitiator());
		trans.setTransTypeID(1);
		trans.setDebtor(command[3]);
		trans.setCreditor(command[1]);
		trans.setAmount(command[6]);
		trans.setState("0");
		trans.setChannelID(Long.parseLong(transactionData.getChannelId()));
		trans.setIsAtomic(false);
		// Connection conn = new ConnectionPool().getConnection();
		try {
			// dataSource =
			// ConnectionPool.setUp(transactionData,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();
			id = transactionDAO.save(conn,
					"insert into Transaction(initiator,transTypeID,lastModified,debtor,creditor,amount,state,resultID,parentID,channelID,isAtomic) values(?,?,?,?,?,?,?,?,?,?,?)",
					new Object[] { trans.getInitiator(), trans.getTransTypeID(), new Date(), trans.getDebtor(),
							trans.getCreditor(), trans.getAmount(), trans.getState(), trans.getResultID(),
							trans.getParentID(), trans.getChannelID(), trans.getIsAtomic() },
					transactionData, propToTransaction);
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return id;
	}

	public long saveFeeTrans(Trans trans, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		long id = 0;
		// Connection conn = new ConnectionPool().getConnection();
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);

			conn = DBPool.getConnection();// dataSource.getConnection();

			transactionDAO = new TransactionDAOImpl();
			if (conn != null)
				id = transactionDAO.save(conn,
						"insert into Transaction(initiator,transTypeID,lastModified,debtor,creditor,amount,state,resultID,parentID,channelID,isAtomic) values(?,?,?,?,?,?,?,?,?,?,?)",
						new Object[] { trans.getInitiator(), trans.getTransTypeID(), new Date(), trans.getDebtor(),
								trans.getCreditor(), trans.getAmount(), trans.getState(), trans.getResultID(),
								trans.getParentID(), trans.getChannelID(), trans.getIsAtomic() },
						transaction, propToTransaction);
			else
				id = 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return id;
	}

	public long saveTransExtDetails(long transID, String request, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {
		long id = 0;
		// Connection conn = new ConnectionPool().getConnection();
		TransExtDetails extDetails = null;
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);

			conn = DBPool.getConnection();// dataSource.getConnection();

			transactionDAO = new TransactionDAOImpl();
			if (conn != null) {
				extDetails = new TransExtDetails();
				extDetails.setTransID(transID);
				extDetails.setRequest(request);
				id = transactionDAO.save(conn, "insert into TransExtDetails(transID,request,last_modified) values(?,?,?)",
						new Object[] { extDetails.getTransID(), extDetails.getRequest(),LocalDateTime.now()
							       .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) }, transaction,
						propToTransaction);
			} else {
				id = 0;
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName()
							+ transaction.getThreadName());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		extDetails = null;
		return id;
	}

	public long saveTransData(long transID, String keyType, String keyVal, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {
		long id = 0;
		// Connection conn = new ConnectionPool().getConnection();
		TransactionData data = null;
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);

			conn = DBPool.getConnection();// dataSource.getConnection();

			transactionDAO = new TransactionDAOImpl();
			if (conn != null) {
				data = new TransactionData();
				data.setTransID(transID);
				data.setKeyType(keyType);
				data.setKeyVal(keyVal);
				id = transactionDAO.save(conn, "insert into Transaction_Data(transID,keyType,keyVal) values(?,?,?)",
						new Object[] { data.getTransID(), data.getKeyType(), data.getKeyVal() }, transaction,
						propToTransaction);
			} else {
				id = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		data = null;
		return id;
	}

	public boolean updateCashinTrans(Trans trans, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;
		// Connection conn = new ConnectionPool().getConnection();
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();
			isUpdate = transactionDAO.update(conn,
					"update Transaction set transTypeID=?,lastModified=?,debtor=?,creditor=?,amount=?,state=?,resultID=?,parentID=?,channelID=?,isAtomic=?,resultDesc=?,param1=? where id=?",
					new Object[] { trans.getTransTypeID(), new Date(), trans.getDebtor(), trans.getCreditor(),
							trans.getAmount(), trans.getState(), trans.getResultID(), trans.getParentID(),
							trans.getChannelID(), trans.getIsAtomic(), trans.getResultDesc(), trans.getParam1(),
							trans.getId() },
					transaction, propToTransaction);
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isUpdate;
	}

	public boolean incorrectAttempts(String entityID, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;
		transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		String strQuery = "update password set incorrectAttempts=(select incorrectAttempts+1 from (select * from password where entityID='"
				+ entityID + "') as atmp)  where entityID='" + entityID + "'";
		isUpdate = transactionDAO.updateById(strQuery);
		return isUpdate;
	}

	public boolean updateIncorrectAttempts(long entityID, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {
		boolean isUpdate = false;
		transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		String strQuery = "update password set incorrectAttempts='0' where entityID="+entityID;
		isUpdate = transactionDAO.updateById(strQuery);
		return isUpdate;
	}
	public boolean updateExtTransDetails(String response, Integer extResult, long transID, Transaction transaction,
			PropToTransaction propToTransaction) {
		boolean isUpdate = false;
		;
		try {
			String qry = "update TransExtDetails set response='" + response + "',extResult='" + extResult
					+ "',last_modified=now()  where transID='" + transID + "'";
			ReadLog4J.writeLog(
					"Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "D", transaction.getTransId() + " ~Update TransExtDetails:" + transaction.getInitiator()
							+qry+"~"+transID);
			isUpdate = transactionDAO.updateById(qry);

		} catch (Exception ex) {
			
			ex.printStackTrace();
		}
		return isUpdate;
	}
	public boolean updateExtTransDetails(String response, Object extResult,String extResultDescription, long transID, Transaction transaction,
			PropToTransaction propToTransaction) {
		boolean isUpdate = false;
		transactionDAO = new TransactionDAOImpl();
		try {
			String qry = "update TransExtDetails set response='" + response +  "',extResultDescription='" + extResultDescription
					+ "',last_modified=now()  where transID='" + transID + "'";
			ReadLog4J.writeLog(
					"Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "D", transaction.getTransId() + " ~Update TransExtDetails:" + transaction.getInitiator()
							+qry+"~"+transID);
			isUpdate = transactionDAO.updateById(qry);

		} catch (Exception ex) {
			
			ex.printStackTrace();
		}
		return isUpdate;
	}
	public boolean updateAllchild(Long transID,String state, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;String strQuery = null;
		transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		if(state!="2" || !state.equals("2")){
			strQuery = "update Transaction set state='state',resultDesc='Failed Due to Parent Transaction'   where parentID='"
					+ transID + "'";
		}else{
			strQuery = "update Transaction set state='state',resultDesc='Success'   where parentID='"
					+ transID + "'";
		}
		ReadLog4J.writeLog(
				"Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "D", transaction.getTransId() + " ~Update Child Transaction:" + transaction.getInitiator()
						+strQuery.toString()+"~"+transID);
		isUpdate = transactionDAO.updateById(strQuery);
		return isUpdate;
	}

	public boolean updateTransStateById(String state, Long transID, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		String strQuery = "update Transaction set state='" + state + "' where id='" + transID + "'";
		isUpdate = transactionDAO.updateById(strQuery);
		return isUpdate;
	}

	public Trans getTransByTransID(Long transID, Transaction transaction, PropToTransaction propToTransaction) {
		Trans trans = null;
		String strQuery = "select t.* from Transaction t where t.id='" + transID + "' and t.initiator='" + transaction.getInitiator() + "' and t.state=1";
		transactionDAO = new TransactionDAOImpl();
		try {
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				trans=new Trans();
				trans.setId(rs.getLong("id"));
				trans.setInitiator(rs.getString("initiator"));
				trans.setTransTypeID(rs.getLong("transTypeID"));
				trans.setCreated(rs.getDate("created"));
				trans.setLastModified(rs.getDate("lastModified"));
				trans.setDebtor(rs.getString("debtor"));
				trans.setCreditor(rs.getString("creditor"));
				trans.setAmount(rs.getString("amount"));
				trans.setState(rs.getString("state"));
				trans.setResultID(rs.getLong("resultID"));
				trans.setParentID(rs.getLong("parentID"));
				trans.setChannelID(rs.getLong("channelID"));
				trans.setParam1(rs.getString("param1"));
				trans.setParam2(rs.getString("param2"));
				trans.setParam3(rs.getString("param3"));
				trans.setRecipient(rs.getString("recipient"));
				trans.setProductID(rs.getString("productID"));
				trans.setIsAtomic(rs.getBoolean("isAtomic"));
			}
		} catch (SQLException e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error", transaction.getTransId() + " ~ E: " + errors.toString()
							+ " \n.initiator(" + transaction.getInitiator() + ")." + transaction.getThreadName());

		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error", transaction.getTransId() + " ~ E: " + errors.toString()
							+ " \n.initiator(" + transaction.getInitiator() + ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return trans;
	}
	public boolean getCheckAddMoneytransID(String transID,String amount,Transaction transaction, PropToTransaction propToTransaction) {
		boolean isAvailable=false;
		String strQuery = "select t.id from Transaction t where t.amount='" + amount + "' and t.id='" + transID + "' and t.initiator='" + transaction.getInitiator() + "' and t.state=1";
		transactionDAO = new TransactionDAOImpl();
		try {
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				isAvailable=true;
			}
		} catch (SQLException e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error", transaction.getTransId() + " ~ E: " + errors.toString()
							+ " \n.initiator(" + transaction.getInitiator() + ")." + transaction.getThreadName());

		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error", transaction.getTransId() + " ~ E: " + errors.toString()
							+ " \n.initiator(" + transaction.getInitiator() + ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isAvailable;
	}
	public long getTransTypeID(String transNameD, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		long transTypeID = 0;
		transactionDAO = new TransactionDAOImpl();
		String strQuery = "select t.* from TransactionTypes t where t.name='" + transNameD + "'";
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);

			conn = DBPool.getConnection();// dataSource.getConnection();

			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transTypeID = rs.getLong("id");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return transTypeID;
	}
	public double getTotalTransAmount(Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		double amount = 0;
		transactionDAO = new TransactionDAOImpl();
		String strQuery = "select sum(amount) as amount from Transaction where initiator='" + transaction.getInitiator() + "' and state in(1,2) and transTypeID in(13,53) and MONTH(created) = MONTH(CURRENT_DATE())";
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);

			conn = DBPool.getConnection();// dataSource.getConnection();

			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				amount = rs.getDouble("amount");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return amount;
	}
	public Trans getTrans(String transID, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {

		transactionDAO = new TransactionDAOImpl();
		Trans trans = null;
		String strQuery = "select t.* from Transaction t where t.id='"+ transID+"' and t.state!=2 and t.initiator='"+transaction.getInitiator()+"' limit 1";
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {

				// transTypeID = rs.getLong("id");
				trans = new Trans(rs.getLong("id"), rs.getString("initiator"), rs.getLong("transTypeID"),
						rs.getTimestamp("created"), rs.getTimestamp("lastModified"), rs.getString("debtor"),
						rs.getString("creditor"), rs.getString("amount"), rs.getString("state"), rs.getLong("resultID"),
						rs.getLong("parentID"), rs.getLong("channelID"), rs.getString("param1"), rs.getString("param2"),
						rs.getString("param3"), rs.getString("recipient"), rs.getString("productID"),
						rs.getBoolean("isAtomic"));
			}
		} catch (Exception e) {
			// StringWriter errors = new StringWriter();
			// e.printStackTrace(new PrintWriter(errors));
			// ReadLog4J.writeLog("Exception_logs/" + GetTimeStamp.getDate() +
			// ".log", Logger.getLogger("EL "), "e",
			// "Error: " + errors.toString() + "~" +
			// transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return trans;
	}
	public Trans getTransByFetchtransId(String transID, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {

		transactionDAO = new TransactionDAOImpl();
		Trans trans = null;
		String strQuery = "select t.* from Transaction t where t.id='"+ transID+"' and t.state=2 and t.initiator='"+transaction.getInitiator()+"' limit 1";
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {

				// transTypeID = rs.getLong("id");
				trans = new Trans(rs.getLong("id"), rs.getString("initiator"), rs.getLong("transTypeID"),
						rs.getTimestamp("created"), rs.getTimestamp("lastModified"), rs.getString("debtor"),
						rs.getString("creditor"), rs.getString("amount"), rs.getString("state"), rs.getLong("resultID"),
						rs.getLong("parentID"), rs.getLong("channelID"), rs.getString("param1"), rs.getString("param2"),
						rs.getString("param3"), rs.getString("recipient"), rs.getString("productID"),
						rs.getBoolean("isAtomic"),rs.getString("resultDesc"));
			}
		} catch (Exception e) {
			// StringWriter errors = new StringWriter();
			// e.printStackTrace(new PrintWriter(errors));
			// ReadLog4J.writeLog("Exception_logs/" + GetTimeStamp.getDate() +
			// ".log", Logger.getLogger("EL "), "e",
			// "Error: " + errors.toString() + "~" +
			// transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return trans;
	}
	public TransactionData getMarginTransData(String transID, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {

		transactionDAO = new TransactionDAOImpl();
		TransactionData trans = null;
		String strQuery = "select td.* from Transaction_Data td, Transaction t where t.id= td.transID and td.transID="+transID+" limit 1";
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				trans = new TransactionData(rs.getLong("transID"), rs.getString("keyType"), rs.getString("keyVal"));
			}
		} catch (Exception e) {

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return trans;
	}
	public boolean updateTrans(Trans trans, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;
		// Connection conn = new ConnectionPool().getConnection();
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();
			isUpdate = transactionDAO.update(conn,
					"update Transaction set transTypeID=?,lastModified=?,debtor=?,creditor=?,recipient=?,amount=?,state=?,resultID=?,parentID=?,channelID=?,isAtomic=?,resultDesc=?,param1=?,param2=? where id=?",
					new Object[] { trans.getTransTypeID(), new Date(), trans.getDebtor(), trans.getCreditor(),trans.getRecipient(),
							trans.getAmount(), trans.getState(), trans.getResultID(), trans.getParentID(),
							trans.getChannelID(), trans.getIsAtomic(), trans.getResultDesc(), trans.getParam1(),trans.getParam2(),
							trans.getId() },
					transaction, propToTransaction);
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isUpdate;
	}
	// public static void main(String[] args) throws SQLException, Exception {
	// Trans trans = new Trans("initiator", 78L, new Date(), new Date(),
	// "debtor", "creditor", "9807", "state", 76L, 89L, 67L, true);
	// int id = new TransactionDaoService().save(trans);
	// System.out.println(id);
	// }
}
