package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Accounts;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Password;
import in.co.triotech.triomoney.model.TransactionLimits;

public class TransactionDAOImpl implements TransactionDAO {
	DataSource dataSource = null;//ConnectionPool.setUp();
	Transaction trans;
	Logger queryLog = Logger.getLogger("QL");
	PropToTransaction propToTransaction;
	
	public TransactionDAOImpl(Transaction trans,PropToTransaction propToTransaction) {
		this.trans = trans;
		this.propToTransaction = propToTransaction;
	}
	public TransactionDAOImpl() {
	}

	@Override
	public long save(Connection conn, String query, Object[] obj,Transaction transaction,PropToTransaction propToTransaction) {

		int count = 0;
		long isUpdate = 0;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
		} catch (SQLException e1) {
			StringWriter errors = new StringWriter();
			e1.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
		}
		ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",transaction.getTransId()+" ~D: "+query.toString()+". initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
		//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", query.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
		ResultSet rs = null;
		try {
			for (int i = 0; i < obj.length; i++)
				ps.setObject(++count, obj[i]);
			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",transaction.getTransId()+" ~ "+ ps.toString()+". initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
			//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", ps.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
			//long start = System.currentTimeMillis();
			ps.executeUpdate();
			//long end = System.currentTimeMillis();
			rs = ps.getGeneratedKeys();
			if (rs.next())
				isUpdate = rs.getInt(1);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
			}
		}
		
		return isUpdate;
	
	}

	@Override
	public boolean update(Connection conn, String query, Object[] obj,Transaction transaction,PropToTransaction propToTransaction) {
		int count = 0;
		boolean isUpdate = false;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(query);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			for (int i = 0; i < obj.length; i++)
				ps.setObject(++count, obj[i]);

			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",transaction.getTransId()+" ~ "+ ps.toString()+". initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
			//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", ps.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
			int i = ps.executeUpdate();
			if (i > 0)
				isUpdate = true;
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+"). "+transaction.getThreadName());
			}
		}
		
		return isUpdate;
	}

	@Override
	public Transaction getTransactionById(String id, String qry)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transaction> getTransactionList(String qry) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet executeQuery(String qry,Statement stm,Transaction transaction,PropToTransaction propToTransaction) {
		ResultSet rs = null;
//		Statement stm = null;
//		Connection con = null;
		try {
//			con = new ConnectionPool().getConnection();
//			stm = con.createStatement();
			
			rs = stm.executeQuery(qry);
			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",transaction.getTransId()+" ~ "+ qry.toString()+". initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
			//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", qry.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
		} catch (Exception e) {
			
		} 
		
		return rs;
	}

	@Override
	public Object getResultObject(String qry) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isValid(String qry) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		boolean isValid = false;
		try {				
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(qry);
			ReadLog4J.writeLog("Worker_Logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ qry.toString()+". initiator("+this.trans.getInitiator()+")."+trans.getThreadName());
			//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", qry,LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
			while(rs.next()){
				isValid = true;
			}
		} catch (Exception e) {
			isValid = false;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). ");
			}
		}
		
		return isValid;
	}
	public long getRowID(String qry) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		long id=0;
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(qry);
			//ReadLog4J.writeLog("Worker_Logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ qry.toString()+". initiator("+this.trans.getInitiator()+").");
			while(rs.next()){
				 id=Long.parseLong(rs.getString("id"));
			}
		} catch (Exception e) {
			id = 0;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). ");
			}
		}
		
		return id;
	}
	public boolean deleteRowByID(String qry) {
		Statement stm = null;
		Connection con = null;
		boolean isDelete=true;
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			stm.executeUpdate(qry);
			ReadLog4J.writeLog("Worker_Logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ qry.toString()+". initiator("+this.trans.getInitiator()+").");
		} catch (Exception e) {
			isDelete = false;
		} 
		finally{
			try{
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). ");
			}
		}
		
		return isDelete;
	}
	@Override
	public TransactionLimits getTransLimit(String circleID, String groupID,
			String transAccountTypeID) {
		return null;
	}

	@Override
	public Entity getEntity(String msisdn)  {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		Entity entity = null;
		String query = "select * from Entity e where e.msisdn='"+msisdn+"'";
				try {
					ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ query.toString()+". initiator("+this.trans.getInitiator()+"). Performed by "
							+ trans.getThreadName());
					//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", query.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while(rs.next()){
				entity = new Entity(rs.getLong("id"), rs.getString("msisdn"), rs.getString("name"), rs.getInt("statusID"), rs.getInt("typeID"), rs.getInt("parentID"), rs.getInt("circleID"), rs.getString("pin"),rs.getString("param1"),rs.getString("param2"),rs.getString("param3"),rs.getString("last_login"));
			}
		} catch (Exception e) {
			entity = null;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). ");
			}
		}
		
		return entity;
	}
	
	@Override
	public Entity getEntity(long entityID)  {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		Entity entity = null;
		String query = "select * from Entity e where e.id='"+entityID+"'";
				try {
					ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ query.toString()+". initiator("+this.trans.getInitiator()+"). Performed by "
							+ trans.getThreadName());
					//ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query", "querylogger", query.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while(rs.next()){
				entity = new Entity(rs.getLong("id"), rs.getString("msisdn"), rs.getString("name"), rs.getInt("statusID"), rs.getInt("typeID"), rs.getInt("parentID"), rs.getInt("circleID"), rs.getString("pin"),rs.getString("param1"),rs.getString("param2"),rs.getString("param3"),rs.getString("last_login"));
			}
		} catch (Exception e) {
			entity = null;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). ");
			}
		}
		
		return entity;
	}

	@Override
	public Password getPassword(String entityId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Accounts getAccounts(String entityID) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		Accounts accounts = null;
		String query = "select * from Accounts a where a.entityID ='" + entityID + "'";
				
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while (rs.next()) {
				accounts = new Accounts(rs.getLong("id"), rs.getLong("entityID"), rs.getDouble("currentBalance"),
						rs.getDouble("availableBalance"), rs.getString("lowerLimit"), rs.getString("upperLimit"),
						rs.getLong("typeID"));
			}
		} catch (Exception e) {
			accounts = null;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). ");
			}
		}
		return accounts;
	}

	@Override
	public void checkFee(String method) {
		if(method.equalsIgnoreCase("drTrans")){
			
		}
		else if(method.equalsIgnoreCase("drWallet")){
			
		}
		else{
			
		}
	}

	public boolean updateById(String query){
		boolean isUpdate = false;
		
		Statement stm = null;
		Connection con = null;
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			int i = stm.executeUpdate(query);
			if(i > 0){
				isUpdate=true;
			}
			
		} catch (Exception e) {
			isUpdate = false;
		} 
		finally{
			try{
			
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). "+trans.getThreadName());
			}
		}
		
		return isUpdate;
	}
	public long getLastTrans(Object reMSISDN, Object recMSISDN) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		long transID = 0;
		
		String query ="select transID from Voucher where fromMSISDN='"+reMSISDN+"' and toMSISDN='"+recMSISDN+"' and status='1' order by transID desc limit 1";
		try {
			ReadLog4J.writeLog("Worker_Logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ query.toString()+". initiator("+this.trans.getInitiator()+")."+trans.getThreadName());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while(rs.next()){
			 transID=rs.getLong("transID");
			}
		} catch (Exception e) {
			transID = 0;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). "+trans.getThreadName());
			}
		}
		
		return transID;
	}
	
	public String getEntityType(long entityId) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String type = null;
		
		String query ="select et.type as type from Entity e, EntityType et where et.id=e.typeID and e.id='"+entityId+"'";
		try {
			ReadLog4J.writeLog("Worker_Logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", queryLog,"info",this.trans.getTransId()+" ~ "+ query.toString()+". initiator("+this.trans.getInitiator()+")."+trans.getThreadName());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();//dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while(rs.next()){
				type=rs.getString("type");
			}
		} catch (Exception e) {
			type = null;
		} 
		finally{
			try{
			if(rs!=null)
				rs.close();
			if(stm!=null)
				stm.close();
			if(con!=null)
				con.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+this.propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",this.trans.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+this.trans.getInitiator()+"). "+trans.getThreadName());
			}
			
		}
		
		return type;
	}
	public static void main(String[] args) throws SQLException {
		
	}

}
