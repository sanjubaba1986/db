package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.MiniStatement;

/**
 * @author Abhay
 *
 */
public class MiniStatementDAO {
	private TransactionDAO transactionDAO = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	private DataSource dataSource = null;// ConnectionPool.setUp();
	Logger accessLog = Logger.getLogger("QL" + MiniStatementDAO.class);

	public List<MiniStatement> getDetail(Long entityID, Transaction transaction, PropToTransaction propToTransaction) {
		List<MiniStatement> details = new ArrayList<MiniStatement>();
		String strQuery = "SELECT t.id,t.amount,t.created,t.recipient,t.productID,t.status,IF(t.d='Send Money' and t.recipient='"
				+ transaction.getInitiator() + "'"
				+ ",'Received Money',d) as type,IF(productID='0','',(SELECT name as product from Product where id=t.productID)) as product from (SELECT t.id,tt.description as d,t.amount,t.created,t.recipient,t.productID,ts.name as status from TransactionTypes tt,Transaction t,TransactionState ts where t.transTypeID=tt.id and t.state =ts.id and tt.isFinancial=1 and t.state!=0 and (t.initiator='"
				+ transaction.getInitiator() + "'" + " or t.recipient='" + transaction.getInitiator() + "'"
				+ ") order by id desc limit 10) as t";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection(); //conn
											// = new
											// ConnectionPool().getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				MiniStatement transDetail = new MiniStatement();
				transDetail.setId(rs.getString("id"));
				transDetail.setType(rs.getString("type"));
				transDetail.setAmount(rs.getString("amount"));
				transDetail.setCreated(rs.getString("created"));
				transDetail.setRecipient(rs.getString("recipient"));
				transDetail.setStatus(rs.getString("status"));
				transDetail.setType(rs.getString("type"));
				if (rs.getString("product") != "" && rs.getString("product") != "NULL") {
					transDetail.setProduct(rs.getString("product"));

				} else {
					transDetail.setProduct("NA");
				}

				details.add(transDetail);

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}
			}
		}

		return details;
	}

}
