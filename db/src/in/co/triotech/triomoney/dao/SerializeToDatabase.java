package in.co.triotech.triomoney.dao;

	import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;

	public class SerializeToDatabase {
		private  TransactionDAO transactionDAO = null;
		private  Connection conn;
		private static final String SQL_SERIALIZE_OBJECT = "INSERT INTO serialized_java_objects(object_name, serialized_object) VALUES (?, ?)";
		private static final String SQL_DESERIALIZE_OBJECT = "SELECT serialized_object FROM serialized_java_objects WHERE serialized_id = ?";

		public  long serializeJavaObjectToDB(Transaction transaction,PropToTransaction propToTransaction) throws SQLException {

			long id = 0;
			try {
				// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
				// conn = new ConnectionPool().getConnection();
				transactionDAO = new TransactionDAOImpl();
				conn = DBPool.getConnection();// dataSource.getConnection();
				id = transactionDAO.save(conn,
						SQL_SERIALIZE_OBJECT,
						new Object[] { transaction.getClass().getName(),transaction },
						transaction, propToTransaction);
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("EL"), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName()
								+ transaction.getThreadName());

			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			return id;
		}

		/**
		 * To de-serialize a java object from database
		 *
		 * @throws SQLException
		 * @throws IOException
		 * @throws ClassNotFoundException
		 */
		public  Object deSerializeJavaObjectFromDB(Connection connection,
				long serialized_id) throws SQLException, IOException,
				ClassNotFoundException {
			transactionDAO = new TransactionDAOImpl();
			ResultSet rs =null;
			try {
				// dataSource = ConnectionPool.setUp(transaction,propToTransaction);

				conn = DBPool.getConnection();// dataSource.getConnection();

				PreparedStatement pstmt = conn
						.prepareStatement(SQL_DESERIALIZE_OBJECT);
				pstmt.setLong(1, serialized_id);
				 rs = pstmt.executeQuery();
				rs.next();
				byte[] buf = rs.getBytes(1);
				ObjectInputStream objectIn = null;
				if (buf != null)
					objectIn = new ObjectInputStream(new ByteArrayInputStream(buf));

				Object deSerializedObject = objectIn.readObject();

				rs.close();
				pstmt.close();

				System.out.println("Java object de-serialized from database. Object: "
						+ deSerializedObject + " Classname: "
						+ deSerializedObject.getClass().getName());
				return deSerializedObject;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				if (conn != null) {
					try {
						conn.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		
			}
}
