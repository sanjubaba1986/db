package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.CurrentTransaction;

public class CurrentTransactionDAO {
	private TransactionDAO transactionDAO = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	//private DataSource dataSource = null;// ConnectionPool.setUp();
	Logger accessLog = Logger.getLogger("QL" + MiniStatementDAO.class);

	public List<CurrentTransaction> getCurrentTransactionList(String transID, Transaction transaction, PropToTransaction propToTransaction) {
		List<CurrentTransaction> currentTransactions = new ArrayList<CurrentTransaction>();
		String strQuery = null;
		if(transID.equals("0") || transID == null  || transID.length() == 1)
			strQuery = "SELECT t.* from currentAggTransaction t  order by t.aggregatorID desc limit 10";
		else
			strQuery = "SELECT t.* from currentAggTransaction t where t.aggregatorID < '"
					+ transID + "' order by t.aggregatorID desc limit 10";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection(); //conn
											// = new
											// ConnectionPool().getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				CurrentTransaction currentTransaction = new CurrentTransaction();
				currentTransaction.setTransactionDate(String.valueOf(rs.getDate("transactionDate")));
				currentTransaction.setClientID(String.valueOf(rs.getLong("clientID")));
				currentTransaction.setAggregatorID(String.valueOf(rs.getLong("aggregatorID")));
				currentTransaction.setRechargeType(rs.getString("RechargeType"));
				currentTransaction.setOperator(rs.getString("operator"));
				currentTransaction.setRecipient(String.valueOf(rs.getLong("recipient")));
				currentTransaction.setPreBalance(String.valueOf(rs.getDouble("PreBalance")));
				currentTransaction.setPurchaseAmount(String.valueOf(rs.getDouble("purchaseAmount")));
				currentTransaction.setBSNLMargin(String.valueOf(rs.getDouble("BSNLMargin")));
				currentTransaction.setAmountDeductedAGG(String.valueOf(rs.getDouble("AmountDeductedAGG")));
				
				currentTransactions.add(currentTransaction);

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + MiniStatementDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}
			}
		}

		return currentTransactions;
	}
}
