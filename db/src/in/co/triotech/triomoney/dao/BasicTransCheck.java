package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Transdetail;

public class BasicTransCheck {
	private TransactionDAO transactionDAO = null;
	private TransactionStatus transStatus = null;
	
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	DataSource dataSource = null;//ConnectionPool.setUp();
	/*
	 * initiator is registered MPIN / PWD / PINCODE is correct 
	 * Financial ------
	 * Sufficient Balance 
	 * MIN MAX Range Limit
	 * 
	 * Fee (Child Transaction )
	 * 
	 * Commission (Child Transaction)
	 */

	public Boolean isInitiatorRegistered(Transaction trans,PropToTransaction propToTransaction,String commandType) {
		boolean isValid = true;
		if(commandType!="register"){
		try {
			transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
			transStatus = new TransactionStatusImpl();
			String query = "select id from Entity where statusID='1' and msisdn="+trans.getInitiator();
			boolean isValidTrans = transactionDAO.isValid(query);
			if (isValidTrans == false) {
				isValid = false;
				transStatus.rollBack("Initiator not registered",trans.getFrom(),trans,propToTransaction);
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		}
		return isValid;
	}

	public Boolean validateAvailableBalance(Transaction trans ,String amount,PropToTransaction propToTransaction) {
			boolean isAllow = false;
			transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
			String query = "select e.id from Entity e ,Accounts a  where a.entityID=e.id and a.availableBalance >= "+amount+" and e.msisdn="+trans.getInitiator();
			boolean isValidTrans = transactionDAO.isValid(query);
			if (isValidTrans) {
				isAllow = true;
			}
		return isAllow;
	}
	public Boolean checkWalletAmountLimit(String amount,long entityID,Transaction trans,PropToTransaction propToTransaction) {
		boolean isAllow = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from Accounts where "+amount+" BETWEEN lowerLimit and upperLimit and entityID="+entityID;
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isAllow = true;
		}
	return isAllow;
    }
	public Boolean isBothSameMobile(String customerMobileNo,String BenificiaryMobileNo) {
		boolean isCorrect = false;
		if (customerMobileNo.equalsIgnoreCase(BenificiaryMobileNo)) {
			isCorrect = true;
		}
	return isCorrect;
	}
	public long checkregisteredCustomer(String msisdn,Transaction trans,PropToTransaction propToTransaction) {
		long entityTypeId=0;
		String strQuery = "select e.typeID from Entity e , EntityType et where et.id= e.typeID and e.msisdn="+msisdn;
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,trans,propToTransaction);
		while(rs.next())
		{
			entityTypeId= Integer.parseInt(rs.getString("typeID"));
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
		}
			return entityTypeId;
   }
	public double checkBalance(String entityId,Transaction trans,PropToTransaction propToTransaction) {
		double balance=0.0;
		String strQuery = "select availableBalance from Accounts where entityID="+entityId;
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,trans,propToTransaction);
		while(rs.next())
		{
			balance= Double.parseDouble(rs.getString("availableBalance"));
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
		}
			return balance;
   }
	
	public Transdetail getTransDetails(String initiator,String tid,Transaction trans,PropToTransaction propToTransaction) {
		Transdetail transdetail=null;
		String strQuery = "select tt.description  as type,ts.name as status from TransactionTypes tt,TransactionState ts,Transaction t where tt.id=t.transtypeID and ts.id=t.state and t.id='"+tid+"' and t.initiator='"+initiator+"'";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,trans,propToTransaction);
		while(rs.next())
		{
			transdetail= new Transdetail();
			transdetail.setType(rs.getString("type"));
			transdetail.setStatus(rs.getString("status"));
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
		}
			return transdetail;
   }
	public Boolean isPincodeCorrect(Transaction trans,String pincode,PropToTransaction propToTransaction) {
		boolean isCorrect = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from Entity where pin="+pincode+" and  msisdn="+ trans.getInitiator();
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isCorrect = true;
		}
		return isCorrect;
	}
	
	public Boolean isPincodeCorrect(String pincode,Transaction trans,PropToTransaction propToTransaction) {
		boolean isCorrect = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select e.id from Entity e,EntityStatusType s where e.statusID = s.id and s.name='Active' and e.pin = '"+pincode+"' limit 1";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isCorrect = true;
		}
		return isCorrect;
	}
	
	public Boolean isMPincodeCorrect(Transaction trans,String pin,PropToTransaction propToTransaction) {
		boolean isCorrect = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select e.id from Entity e ,password p where p.entityID=e.id and p.password=sha1('"+pin+"') and e.msisdn="+ trans.getInitiator()+" and  e.statusID='1'";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isCorrect = true;
		}
	return isCorrect;
	}
	public Boolean checkIncorrectAttempts(Transaction trans,String incorrectAttempts,String pin,PropToTransaction propToTransaction) {
		boolean isCorrect = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select e.id from Entity e ,password p where p.entityID=e.id and p.incorrectAttempts="+incorrectAttempts+"  and e.msisdn="+ trans.getInitiator()+" and  e.statusID='1'";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isCorrect = true;
		}
	return isCorrect;
	}
	public Boolean checkPasswordExpired(Transaction trans,long entityID,PropToTransaction propToTransaction) {
		boolean isCorrect = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from password where Date(lastModified) <= DATE_SUB(now(),INTERVAL 90 DAY) and entityID='"+entityID+"'";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isCorrect = true;
		}
	return isCorrect;
	}
	public int isCheckEntityType(String msisdn,Transaction trans,PropToTransaction propToTransaction) {
		int entityTypeID=0;
		String strQuery = "select et.id from EntityType et,Entity e where e.typeID=et.id and e.statusID='1' and  e.msisdn="+msisdn;
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,trans,propToTransaction);
		while(rs.next())
		{
			entityTypeID= Integer.parseInt(rs.getString("id"));
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
		}
			return entityTypeID;
   }
	public Boolean isCashinInitiate(String transID,Transaction trans,PropToTransaction propToTransaction) {
		boolean isInitiate = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from otp where transID="+transID+" and status='1' and  expiry_date > now()";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isInitiate = true;
		}
	return isInitiate;
	}
	public boolean isFeeAllow(String transTypeID,Transaction trans,PropToTransaction propToTransaction) {
		boolean isAllow = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from Fee where transTypeID="+transTypeID;
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isAllow = true;
		}
	return isAllow;
	}
	
	public boolean isCommisionProduct(String productID,Transaction trans,PropToTransaction propToTransaction) {
		boolean isAllow = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from Commission where productID="+productID;
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isAllow = true;
		}
	return isAllow;
	}
	
	public boolean isCommisionTransType(String transTypeID,Transaction trans,PropToTransaction propToTransaction) {
		boolean isAllow = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from Commission where transTypeID="+transTypeID;
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isAllow = true;
		}
	return isAllow;
	}
	public Integer isOtpValid(String otp,String transID,Transaction trans,PropToTransaction propToTransaction) {
		Integer id = 0;
		transactionDAO = new TransactionDAOImpl();
		String query = "select id from otp where otp='"+otp+"' and transID='"+transID+"' and  expiry_date > now()";
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(query,stm,trans,propToTransaction);
		
			while(rs.next())
			{
				id = rs.getInt("id");
			}
		} 
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
		}
		
	return id;
	}
	public Boolean checkAmountLimit(Transaction trans,String amount,PropToTransaction propToTransaction) {
		boolean isamount = false;
		try {
			transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
			String query = "select e.id from Entity e,EntityGroupMapping egm,Accounts a,TransactionLimits tl where e.id=a.entityID and e.id=egm.entityID and a.typeID=tl.transAccountType and e.circleID=tl.circleID and tl.groupId=egm.groupID and e.msisdn="+trans.getInitiator()+" and "+amount+" between tl.minAmount and tl.maxAmount";
			boolean isValidTrans = transactionDAO.isValid(query);
			if (isValidTrans) {
				isamount = true;				
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		return isamount;
	}
	

}
