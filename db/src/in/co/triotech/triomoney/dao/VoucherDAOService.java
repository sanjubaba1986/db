package in.co.triotech.triomoney.dao;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Formatter;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Voucher;

public class VoucherDAOService {
	private TransactionDAO  transactionDAO=null;
	private Voucher voucher = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	private DataSource dataSource = null;//ConnectionPool.setUp();
	public long createVoucher(long entityID,Trans trans, double voucherAmount,Object from,Object to, String idType, String idNumber,Object targetPincode,Transaction transaction,PropToTransaction propToTransaction)
	{
		long id = 0;String secretKey="";
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
		 conn = DBPool.getConnection();//dataSource.getConnection();
		transactionDAO = new TransactionDAOImpl();
	
		secretKey=encryptIDNumber(idNumber,transaction,propToTransaction);
		
		id = transactionDAO.save(conn, "insert into Voucher(transID,lastModified,idType,idNumber,entityID,status,fromMSISDN,toMSISDN,fromName,toName,amount,targetPIN) values(?,?,?,?,?,?,?,?,?,?,?,?)", new Object[]{trans.getId(),new Date(),idType,secretKey,entityID,"1",trans.getDebtor(),trans.getCreditor(),from,to,voucherAmount,targetPincode},transaction,propToTransaction);
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		finally{
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
		}
		
		return id;
	}
	
	public long getDynamicVoucherID(long idlength)
	{
		String strlength = "9";
		for(int i=0;i<idlength;i++)
			strlength+="0";
		
		return (long) Math.round(Math.random() * Long.parseLong(strlength));
	}
	
	public Voucher getVoucherByVoucherID(Long voucherID,Transaction transaction,PropToTransaction propToTransaction)
	{
		String strQuery = "select v.* from Voucher v where v.id='"+voucherID+"'";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,transaction,propToTransaction);
		while(rs.next())
		{
			voucher = new Voucher(rs.getLong("id"), rs.getLong("transID"), rs.getDate("created"), rs.getDate("lastModified"),rs.getString("idType"),rs.getString("idNumber"), rs.getLong("entityID"), rs.getString("status"), rs.getString("fromMSISDN"), rs.getString("toMSISDN"), rs.getString("fromName"), rs.getString("toName"), rs.getString("amount"), rs.getString("targetPIN"), rs.getString("cashoutTID"), rs.getString("param1"), rs.getString("param2"), rs.getString("param3"));
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
		}
		System.out.print("getVoucherByVoucherID");
		
		return voucher;
	}
	
	public Voucher getVoucherAmountByTransID(String transID,Transaction transaction,PropToTransaction propToTransaction)
	{
		String strQuery = "select * from Voucher  where transID='"+transID+"'";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,transaction,propToTransaction);
		while(rs.next())
		{
			voucher = new Voucher(rs.getLong("id"), rs.getLong("transID"), rs.getDate("created"), rs.getDate("lastModified"),rs.getString("idType"),rs.getString("idNumber"), rs.getLong("entityID"), rs.getString("status"), rs.getString("fromMSISDN"), rs.getString("toMSISDN"), rs.getString("fromName"), rs.getString("toName"), rs.getString("amount"), rs.getString("targetPIN"), rs.getString("cashoutTID"), rs.getString("param1"), rs.getString("param2"), rs.getString("param3"));
		}
	}
	catch(Exception e){
		StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

	}
	finally{
		if(rs!=null){
			try{
				rs.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
		if(stm!=null){
			try{
				stm.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
		if(conn!=null){
			try{
				conn.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
	}
		
		return voucher;
	}
	
	
	public String getVoucherAmountByTransID(String transID,String validity,Transaction transaction,PropToTransaction propToTransaction)
	{
		String amount=null;
		String strQuery = "select amount from Voucher where transID='"+transID+"' and (cashoutTID is null or cashoutTID=0) and DATE_ADD(lastModified,INTERVAL '"+validity+"' DAY) >= now()";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,transaction,propToTransaction);
		while(rs.next())
		{
			amount=rs.getString("amount");
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

				}
			}
		}
		
		return amount;
	}
	
	public String getVoucherAmountByTransID(String transID,String validityFrom,String validityTo,Transaction transaction,PropToTransaction propToTransaction)
	{
		String amount=null;
		String strQuery = "select amount from Voucher where transID='"+transID+"' and (cashoutTID is null or cashoutTID=0) and now() between DATE_ADD(lastModified,INTERVAL '"+validityFrom+"' DAY) and DATE_ADD(lastModified,INTERVAL '"+validityTo+"' DAY)";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,transaction,propToTransaction);
		while(rs.next())
		{
			amount=rs.getString("amount");
		}
	}
	catch(Exception e){
		StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

	}
	finally{
		if(rs!=null){
			try{
				rs.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
		if(stm!=null){
			try{
				stm.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
		if(conn!=null){
			try{
				conn.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
	}
		
		return amount;
	}
	
	public boolean isVoucherExpired(String transID,int expiry1,Transaction transaction,PropToTransaction propToTransaction)
	{
		boolean isExpire = true;
		String strQuery = "select amount from Voucher where transID='"+transID+"' and (cashoutTID is null or cashoutTID=0) and now() <= DATE_ADD(lastModified,INTERVAL '"+expiry1+"' DAY)";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,transaction,propToTransaction);
		while(rs.next())
		{
			isExpire=false;
		}
	}
	catch(Exception e){
		StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

	}
	finally{
		if(rs!=null){
			try{
				rs.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
		if(stm!=null){
			try{
				stm.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
		if(conn!=null){
			try{
				conn.close();
			}
			catch(Exception e){
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

			}
		}
	}
		
		return isExpire;
	}
	
	public Boolean isValidSecretkey(String transID, String secretkey,Transaction transaction,PropToTransaction propToTransaction)
	{
		boolean isAllow = false;
		transactionDAO = new TransactionDAOImpl(transaction,propToTransaction);
		String query = "select id from Voucher  where transID='"+transID+"' and idNumber=sha1('"+secretkey+"') ";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isAllow = true;
		}
	    return isAllow;
	}
	public boolean updateCashoutId(Integer cashoutTransId , long Id,long entityID,Transaction transaction,PropToTransaction propToTransaction)
	{
		boolean isUpdate=false;
		transactionDAO = new TransactionDAOImpl(transaction,propToTransaction);
		boolean transQuery = false;
		try {
			transQuery = transactionDAO.updateById("update Voucher set cashoutTID="+cashoutTransId+" ,entityID="+entityID+" where id="+Id+"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		if(transQuery){
			isUpdate=true;
		}else{
			isUpdate=false;
		}
		return isUpdate;
	}
	
	public boolean updateVoucherCashoutID(String cashoutId , Long transID,Transaction transaction,PropToTransaction propToTransaction)
	{
		boolean isUpdate=false;
		transactionDAO = new TransactionDAOImpl(transaction,propToTransaction);
		boolean transQuery=transactionDAO.isValid("update Voucher set cashoutTID='"+cashoutId+"' where transID='"+transID+"'");
		if(transQuery){
			isUpdate=true;
		}else{
			isUpdate=false;
		}
		return isUpdate;
	}
	public boolean updateTransStateById(Integer state ,String transID,Transaction transaction,PropToTransaction propToTransaction)
	{
		transactionDAO = new TransactionDAOImpl(transaction,propToTransaction);
		boolean isUpdate = false;
		try {
			isUpdate = transactionDAO.updateById("update Transaction set state='"+state+"' where id='"+transID+"'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		
		return isUpdate;
	}
	public boolean updateVoucherID(String idNumber ,long id ,Transaction transaction,PropToTransaction propToTransaction)
	{
		transactionDAO = new TransactionDAOImpl(transaction,propToTransaction);
		boolean isUpdate = false;
		try {
			isUpdate = transactionDAO.updateById("update Voucher  set idNumber=sha1('"+idNumber+"')   where id='"+id+"'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		
		return isUpdate;
	}
	private static String encryptIDNumber(String password,Transaction transaction,PropToTransaction propToTransaction)
	{
	    String sha1 = "";
	    try
	    {
	        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
	        crypt.reset();
	        crypt.update(password.getBytes("UTF-8"));
	        sha1 = byteToHex(crypt.digest());
	    }
	    catch(NoSuchAlgorithmException e)
	    {
	        StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

	    }
	    catch(UnsupportedEncodingException e)
	    {
	        StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error",transaction.getTransId()+" ~ E: "+errors.toString()+"  \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

	    }
	    return sha1;
	}

	private static String byteToHex(final byte[] hash)
	{
	    Formatter formatter = new Formatter();
	    for (byte b : hash)
	    {
	        formatter.format("%02x", b);
	    }
	    String result = formatter.toString();
	    formatter.close();
	    return result;
	}
}
