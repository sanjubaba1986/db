package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.EntityData;

public class EntityDAOService {
	private TransactionDAO transactionDAO = null;
	private Entity entity = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	private DataSource dataSource = null;// ConnectionPool.setUp();
	Logger accessLog = Logger.getLogger("QL" + EntityDAOService.class);
	Logger excepLog = Logger.getLogger("EL" + EntityDAOService.class);

	public Entity getEntity(Transaction transaction, PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		Entity entity = null;
		String query = "select * from Entity e where e.msisdn='" + transaction.getInitiator() + "' and e.statusID='1'";
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~ " + query.toString() + ". initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
			// ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query",
			// "querylogger",
			// query.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while (rs.next()) {
				entity = new Entity(rs.getLong("id"), rs.getString("msisdn"), rs.getString("name"),
						rs.getInt("statusID"), rs.getInt("typeID"), rs.getInt("parentID"), rs.getInt("circleID"),
						rs.getString("pin"), rs.getString("param1"), rs.getString("param2"), rs.getString("param3"),
						rs.getString("last_login"));
			}
		} catch (Exception e) {
			entity = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("EL"), "error", transaction.getTransId() + " ~ E: " + errors.toString()
								+ " \n.initiator(" + transaction.getInitiator() + "). ");
			}
		}

		return entity;
	}

	public EntityData getEntityData(long entityID, Transaction transaction, PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		EntityData entityData = null;
		String query = "select * from EntityData  where entityID=" + entityID;
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~ " + query.toString() + ". initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
			// ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query",
			// "querylogger",
			// query.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(query);
			while (rs.next()) {
				entityData = new EntityData(rs.getLong("id"), rs.getLong("entityID"), rs.getTimestamp("regDate"),
						rs.getString("email"));
			}
		} catch (Exception e) {
			entity = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("EL"), "error", transaction.getTransId() + " ~ E: " + errors.toString()
								+ " \n.initiator(" + transaction.getInitiator() + "). ");
			}
		}

		return entityData;
	}

	public Entity getEntityByEntityID(Long entityID, Transaction transaction, PropToTransaction propToTransaction) {
		String strQuery = "select * from Entity e where e.id ='" + entityID + "' and e.statusID='1'";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection(); //conn
											// = new
											// ConnectionPool().getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				entity = new Entity();
				entity.setId(rs.getLong("id"));
				entity.setMsisdn(rs.getString("msisdn"));
				entity.setName(rs.getString("name"));
				entity.setStatusID(rs.getLong("statusID"));
				entity.setTypeID(rs.getLong("typeID"));
				entity.setParentID(rs.getLong("parentID"));
				entity.setCircleID(rs.getLong("circleID"));
				entity.setPin(rs.getString("pin"));
				entity.setParam1(rs.getString("param1"));
				entity.setParam1(rs.getString("param2"));
				entity.setParam1(rs.getString("param3"));
				entity.setLast_login(rs.getString("last_login"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + EntityDAOService.class), "error",
					transaction.getTransId() + "~E~" + errors.toString() + " \n.initiator(" + transaction.getInitiator()
							+ ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + EntityDAOService.class), "error",
							transaction.getTransId() + "~E~" + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + EntityDAOService.class), "error",
							transaction.getTransId() + "~E~" + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + EntityDAOService.class), "error",
							transaction.getTransId() + "~E~" + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}
			}
		}

		return entity;
	}

	public boolean setUserKey(long id, String userKey, Transaction transaction, PropToTransaction propToTransaction) {
		boolean isUpdate = false;

		Statement stm = null;
		Connection con = null;
		try {
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			int i = stm.executeUpdate("update Entity set param1='" + userKey + "' where id='" + id + "'");
			try {
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~ " + ". initiator("
								+ transaction.getInitiator() + "). " + transaction.getThreadName());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (i > 0) {
				isUpdate = true;
			}

		} catch (Exception e) {
			isUpdate = false;
		} finally {
			try {

				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("EL"), "error", transaction.getTransId() + "~E~" + errors.toString()
								+ " \n.initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			}
		}
		return isUpdate;

	}

	public boolean setLastLogin(long id, Transaction transaction, PropToTransaction propToTransaction) {
		boolean isUpdate = false;

		Statement stm = null;
		Connection con = null;
		try {
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			int i = stm.executeUpdate("update Entity set last_login=NOW() where id='" + id + "'");
			try {
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~ " + ". initiator("
								+ transaction.getInitiator() + "). " + transaction.getThreadName());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (i > 0) {
				isUpdate = true;
			}

		} catch (Exception e) {
			isUpdate = false;
		} finally {
			try {

				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("EL"), "error", transaction.getTransId() + "~E~" + errors.toString()
								+ " \n.initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			}
		}
		return isUpdate;

	}

	public boolean updateEntity(Entity entity, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;
		// Connection conn = new ConnectionPool().getConnection();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I:  Update User Key for ("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();
			isUpdate = transactionDAO.update(conn,
					"update Entity set statusID=?,parentID=?,pin=?,param1=?,param2=?,param3=?,last_login=? where id=?",
					new Object[] { entity.getStatusID(), entity.getParentID(), entity.getPin(), entity.getParam1(),
							entity.getParam2(), entity.getParam3(), entity.getLast_login(), entity.getId() },
					transaction, propToTransaction);
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isUpdate;
	}
}
