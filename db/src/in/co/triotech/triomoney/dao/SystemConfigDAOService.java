package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.SystemConfig;
import in.co.triotech.model.Transaction;

public class SystemConfigDAOService {
	private TransactionDAO  transactionDAO=null;
	private SystemConfig systemConfig = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	DataSource dataSource = null;//ConnectionPool.setUp();
	
	
	public List<SystemConfig> getSystemConfigList(Transaction trans,PropToTransaction propToTransaction)
	{
		List<SystemConfig> sysConfigList = new ArrayList<>();
		
		String strQuery = "select * from SystemConfig";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();//new ConnectionPool().getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,trans,propToTransaction);
		while(rs.next())
		{
			systemConfig = new SystemConfig(rs.getLong("id"),rs.getString("sys_key"),rs.getString("sys_val"));
			sysConfigList.add(systemConfig);
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error"," ~ E: "+errors.toString()+  trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
				}
			}
		}
		return sysConfigList;
	}
	
	public String getSystemConfigFromList(List<SystemConfig> systemConfigs,String key)
	{
		SystemConfig systemConfig = null;
		String sysValue =null;
		
		Iterator<SystemConfig> iterator = systemConfigs.iterator();
		while(iterator.hasNext())
		{
			systemConfig = (SystemConfig)iterator.next();
			if(systemConfig.getSys_key().equalsIgnoreCase(key))
			{
				sysValue = systemConfig.getSys_val();
			}
		}
		return sysValue;
	}

}
