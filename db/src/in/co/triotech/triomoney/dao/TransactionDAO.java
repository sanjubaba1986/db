package in.co.triotech.triomoney.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import in.co.triotech.model.Accounts;
import in.co.triotech.model.Entity;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Password;
import in.co.triotech.triomoney.model.TransactionLimits;

public interface TransactionDAO {
public long save(Connection conn,String query, Object[] obj,Transaction transaction,PropToTransaction propToTransaction) throws Exception;
public boolean update(Connection conn,String query, Object[] obj,Transaction transaction,PropToTransaction propToTransaction) throws Exception;
public Transaction getTransactionById(String id,String qry) throws Exception;
public List<Transaction> getTransactionList(String qry) throws Exception;
public ResultSet executeQuery(String qry,Statement stm,Transaction transaction,PropToTransaction propToTransaction );
public Object getResultObject(String qry);
public boolean isValid(String qry);
public TransactionLimits getTransLimit(String circleID,String groupID, String transAccountTypeID);
public Entity getEntity(String msisdn);
public Entity getEntity(long entityID);
public Password getPassword(String entityId);
public Accounts getAccounts(String entityID);
public String getEntityType(long entityId);
public void checkFee(String method);
public boolean updateById(String query) throws Exception;
public long getLastTrans(Object reMSISDN, Object recMSISDN);
public long getRowID(String qry);
public boolean deleteRowByID(String qry);
//ResultSet executeQuery(String qry, Statement stm, Connection con);
}
