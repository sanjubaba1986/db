package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.connectivity.PropertyToMap;
import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Accounts;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Commission;
import in.co.triotech.triomoney.model.EntityGroup;
import in.co.triotech.triomoney.model.EntityGroupMapping;
import in.co.triotech.triomoney.model.ProductCommission;

public class CommissionDAOService {
	PropertyToMap propertyToMap = new PropertyToMap();

	DataSource dataSource = null;
	List<Commission> commissionList = null;
	Logger accessLog = Logger.getLogger("WL" + CommissionDAOService.class);

	public ProductCommission getProductCommission(Entity entity, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		ProductCommission productCommission = null;

		StringBuilder queryBuilder = new StringBuilder();

		if (trans.getProductID() != null && trans.getProductID() != "0" && trans.getProductID() != "") {
			strQuery = "select * from ProductCommission where transTypeID='" + trans.getTransTypeID()
					+ "' and productID='" + trans.getProductID() + "' and " + trans.getAmount()
					+ " between fromAmount and toAmount ";
		} else {
			strQuery = "select * from ProductCommission where transTypeID='" + trans.getTransTypeID() + "' and "
					+ trans.getAmount() + " between fromAmount and toAmount ";
		}

		queryBuilder.append(strQuery);
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				productCommission = new ProductCommission(rs.getInt("id"), rs.getInt("transTypeID"),
						rs.getInt("productID"), rs.getString("channelCommissionPercentage"),
						rs.getString("channelCommissionAmount"), rs.getString("created"), rs.getLong("createdBy"),
						rs.getBoolean("isActive"), rs.getString("updated"), rs.getLong("updatedBy"),
						rs.getString("fromAmount"), rs.getString("toAmount"));
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return productCommission;
	}
	public double commissionDeduction(Commission commission, Transaction transaction, BlockingQueue<Transaction> queue,
			Trans trans, double voucherAmount, PropToTransaction propToTransaction) {

		/*
		 * 1) Check method for margin / commission If method is margin then get
		 * the commAmount(margin) and commPercentage(margin) and deduct the
		 * margin from the voucher amount
		 * 
		 * 2)Otherwise Check the commission percentage to get amount as per
		 * commission and amount too
		 * 
		 */
		
		
		 //Checking method for margin / commission Start
		 
		if (commission.getMethod() != null) {

			if (commission.getMethod().equalsIgnoreCase("margin")) {
				
				// returning remaining amount after deducting the margin
				
				return manageMargin(commission,transaction,trans,voucherAmount,propToTransaction);
			}
			else if (commission.getMethod().equalsIgnoreCase("commission")) {
				// Check the commission percentage to get amount as per commission and amount too
				 
			}
		}

		/*
		 * End Checking method for margin / commission
		 */
		
		
		return 0.0;

	}
	public EntityGroupMapping getEntityGroupMapping(Entity entity, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		EntityGroupMapping entityGroupMapping = null;

		strQuery = "select egm.* from EntityGroupMapping egm,EntityGroup eg where egm.groupID=eg.id and egm.entityID!=eg.ownerID and egm.entityID='"
				+ entity.getId() + "'";

		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				entityGroupMapping = new EntityGroupMapping(rs.getLong("id"), rs.getLong("entityID"),
						rs.getLong("groupID"));
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return entityGroupMapping;
	}
	
	public EntityGroup getEntityGroup(String strQuery, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		EntityGroup entityGroup = null;

		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				entityGroup = new EntityGroup(rs.getLong("id"), rs.getString("name"), rs.getString("description"), rs.getInt("ownerID"), rs.getString("type"));		
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return entityGroup;
	}

	public EntityGroup getEntityGroup(Entity entity, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		EntityGroup entityGroup = null;
		
			strQuery = "select eg.* from EntityGroup eg where eg.ownerID='"+ entity.getParentID() + "' and name='Bronze' and type = 'Retailer'";

		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				entityGroup = new EntityGroup(rs.getLong("id"), rs.getString("name"), rs.getString("description"), rs.getInt("ownerID"), rs.getString("type"));		
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return entityGroup;
	}
	
	public EntityGroup getEntityGroup(Entity entity, String name, String type, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		EntityGroup entityGroup = null;
		
			strQuery = "select eg.* from EntityGroup eg where eg.ownerID='"+ entity.getParentID() + "' and name='"+name+"' and type = '"+type+"'";

		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				entityGroup = new EntityGroup(rs.getLong("id"), rs.getString("name"), rs.getString("description"), rs.getInt("ownerID"), rs.getString("type"));		
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return entityGroup;
	}
	
	public EntityGroup getEntityGroupParent(Entity entityParent, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		EntityGroup entityGroup = null;
		
			strQuery = "select eg.* from EntityGroup eg where eg.ownerID='"+ entityParent.getId() + "' and name='Bronze' and type = 'RD'";

		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				entityGroup = new EntityGroup(rs.getLong("id"), rs.getString("name"), rs.getString("description"), rs.getInt("ownerID"), rs.getString("type"));		
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return entityGroup;
	}

	public Commission getCommissionInfo(Entity entity, EntityGroupMapping egm, Transaction transaction, Trans trans, PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		String commtarget =null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		Commission commission = null;
		if(entity.getTypeID() == 2)
			commtarget = "initiator";
		else
			commtarget = "initiator_parent";

		StringBuilder queryBuilder = new StringBuilder();

		if (trans.getProductID() != null && trans.getProductID() != "0" && trans.getProductID() != "") {
			strQuery = "select c.* from Commission c,Entity e,EntityData ed where c.method='commission' and c.transTypeID='"
					+ trans.getTransTypeID() + "' and c.productID='" + trans.getProductID() + "' and c.groupID='"
					+ egm.getGroupID() + "' and commtarget='"+commtarget+"'  and " + trans.getAmount() + " between c.fromAmount and c.toAmount limit 1";
		} else {
			strQuery = "select c.* from Commission c,Entity e,EntityData ed where c.method='commission' and c.transTypeID='"
					+ trans.getTransTypeID() + "' and c.groupID='" + egm.getGroupID() + "' and commtarget='"+commtarget+"' and " + trans.getAmount()
					+ " between c.fromAmount and c.toAmount limit 1";
		}

		queryBuilder.append(strQuery);
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				commission = new Commission();
				commission.setId(rs.getLong("id"));
				commission.setTransTypeID(rs.getLong("transTypeID"));
				commission.setChannelID(rs.getLong("channelID"));
				commission.setCircleID(rs.getLong("circleID"));
				commission.setGroupID(rs.getLong("groupID"));
				commission.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				commission.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				commission.setMethod(rs.getString("method"));
				commission.setFromAmount(rs.getString("fromAmount"));
				commission.setToAmount(rs.getString("toAmount"));
				commission.setCommAmount(rs.getString("commAmount"));
				commission.setCommPercentage(rs.getString("commPercentage"));
				commission.setCommSource(rs.getString("commSource"));
				commission.setCommtarget(rs.getString("commtarget"));
				// commission.setPan(rs.getString("pan"));
				// commission.setGstn(rs.getString("gstn"));
			}
		} catch (Exception e) {
			commission = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return commission;
	}
	
	public Commission getCommissionInfo(Entity entity, EntityGroup eg, Transaction transaction, Trans trans, PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		String commtarget =null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		Commission commission = null;
		if(entity.getTypeID() == 2)
			commtarget = "initiator_parent";
		else
			commtarget = "initiator_franchise";
			
		StringBuilder queryBuilder = new StringBuilder();

		if (trans.getProductID() != null && trans.getProductID() != "0" && trans.getProductID() != "") {
			strQuery = "select c.*,(select ed.pan from EntityData ed where entityID='"+entity.getParentID()+"') pan,(select ed.gstn from EntityData ed where entityID='"+entity.getParentID()+"') gstn from Commission c where c.method='commission' and c.transTypeID='"
					+ trans.getTransTypeID() + "' and c.productID='" + trans.getProductID() + "' and c.groupID='"
					+ eg.getId() + "' and commtarget='"+commtarget+"' and " + trans.getAmount() + " between c.fromAmount and c.toAmount limit 1";
		} else {
			strQuery = "select c.*,(select ed.pan from EntityData ed where entityID='"+entity.getParentID()+"') pan,(select ed.gstn from EntityData ed where entityID='"+entity.getParentID()+"') gstn from Commission c where c.method='commission' and c.transTypeID='"
					+ trans.getTransTypeID() + "' and c.groupID='" + eg.getId() + "' and commtarget='"+commtarget+"'  and " + trans.getAmount()
					+ " between c.fromAmount and c.toAmount limit 1";
		}

		queryBuilder.append(strQuery);
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				commission = new Commission();
				commission.setId(rs.getLong("id"));
				commission.setTransTypeID(rs.getLong("transTypeID"));
				commission.setChannelID(rs.getLong("channelID"));
				commission.setCircleID(rs.getLong("circleID"));
				commission.setGroupID(rs.getLong("groupID"));
				commission.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				commission.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				commission.setMethod(rs.getString("method"));
				commission.setFromAmount(rs.getString("fromAmount"));
				commission.setToAmount(rs.getString("toAmount"));
				commission.setCommAmount(rs.getString("commAmount"));
				commission.setCommPercentage(rs.getString("commPercentage"));
				commission.setCommSource(rs.getString("commSource"));
				commission.setCommtarget(rs.getString("commtarget"));
				commission.setPan(rs.getString("pan"));
				commission.setGstn(rs.getString("gstn"));
			}
		} catch (Exception e) {
			commission = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return commission;
	}

	public List<EntityGroupMapping> getEntityGroupMappingList(Entity entity, Entity parentEntity,
			Transaction transaction, Trans trans, PropToTransaction propToTransaction) {
		List<EntityGroupMapping> entityGroupMappings = new ArrayList<EntityGroupMapping>();
		EntityGroupMapping entityGroupMapping = null;
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
						+ "~Getting group for commission for entityID " + entity.getId());
		entityGroupMapping = getEntityGroupMapping(entity, transaction, trans,
				propToTransaction);
		if (entityGroupMapping != null) {
			entityGroupMappings.add(entityGroupMapping);

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info",
					transaction.getTransId() + "~" + transaction.getThreadName() + "~Adding groupID ("
							+ entityGroupMapping.getGroupID() + ") to list of entityID " + entity.getId());

			if (parentEntity.getTypeID() == 3) {
				entityGroupMapping = getEntityGroupMapping(parentEntity, transaction, trans,
						propToTransaction);
				entityGroupMappings.add(entityGroupMapping);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("WL"), "info",
						transaction.getTransId() + "~" + transaction.getThreadName() + "~Adding groupID ("
								+ entityGroupMapping.getGroupID() + ") to list of RD entityID " + entity.getId());
			}
		} else if(parentEntity.getTypeID() == 9){
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
							+ "~group not defined for the entityID " + entity.getId());
			return null;
		}
		else if(parentEntity.getTypeID() == 3)
		{
			entityGroupMapping = getEntityGroupMapping(parentEntity, transaction, trans,
					propToTransaction);
			entityGroupMappings.add(entityGroupMapping);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info",
					transaction.getTransId() + "~" + transaction.getThreadName() + "~Adding groupID ("
							+ entityGroupMapping.getGroupID() + ") to list of RD entityID " + entity.getId());
		}

		return entityGroupMappings;
	}
	
	public long save(EntityGroupMapping entityGroupMapping , Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		Connection conn = null;
		TransactionDAO transactionDAO = null;
		long id = 0;
		try {
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();

			id = transactionDAO.save(conn,
					"insert into EntityGroupMapping(entityID,groupID) values(?,?)",
					new Object[] { entityGroupMapping.getEntityID(), entityGroupMapping.getGroupID() },
					transaction, propToTransaction);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName()
							+ transaction.getThreadName());

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return id;
	}
	
	public long save(Commission commission , Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		Connection conn = null;
		TransactionDAO transactionDAO = null;
		long id = 0;
		try {
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();

			id = transactionDAO.save(conn,
					"insert into Commission(transTypeID,channelID,circleID,groupID,productID,transAccountTypeID,targetAccountTypeID,method,fromAmount,toAmount,commAmount,commPercentage,commtarget,commSource) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new Object[] { commission.getTransTypeID(), commission.getChannelID(),commission.getCircleID(),commission.getGroupID(),commission.getProductID(),commission.getTransAccountTypeID(),commission.getMethod(),commission.getFromAmount(),commission.getToAmount(),commission.getCommAmount(),commission.getCommPercentage(),commission.getCommtarget(),commission.getCommSource() },
					transaction, propToTransaction);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName()
							+ transaction.getThreadName());

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return id;
	} 
	
	public double getCommissionGroupWise(Entity entity,EntityGroup eg,String totcomm,Trans trans,Transaction transaction,PropToTransaction propToTransaction)
	{
		double commShare = 0.0;
		switch (eg.getName().toLowerCase())
		{
		case "silver" : 
			if(entity.getTypeID() == 2)
			{
				double rdCommShare = (Double.parseDouble(totcomm) * 0.6);
				commShare = (rdCommShare* 0.6);
			}
			else if(entity.getTypeID() == 3)
			{
				commShare = (Double.parseDouble(totcomm) * 0.6);
			}
			else if(entity.getTypeID() == 9)
			{
				commShare = (Double.parseDouble(totcomm) * 0.4);
			}
		break;
		case "gold" : if(entity.getTypeID() == 2)
		{
			double rdCommShare = (Double.parseDouble(totcomm) * 0.7);
			commShare = (rdCommShare* 0.7);
		}
		else if(entity.getTypeID() == 3)
		{
			commShare = (Double.parseDouble(totcomm) * 0.7);
		}
		else if(entity.getTypeID() == 9)
		{
			commShare = (Double.parseDouble(totcomm) * 0.3);
		}
	break;
		case "bronze" :
		default : 
			if(entity.getTypeID() == 2)
			{
				double rdCommShare = (Double.parseDouble(totcomm) * 0.5);
				commShare = (rdCommShare* 0.5);
			}
			else if(entity.getTypeID() == 3)
			{
				commShare = (Double.parseDouble(totcomm) * 0.5);
			}
			else if(entity.getTypeID() == 9)
			{
				commShare = (Double.parseDouble(totcomm) * 0.5);
			}
		break;
		}
		
		return (double) Math.round(commShare * 10000) / 10000;
	}
	
	public long setAndCreateCommission(ProductCommission pc, Entity entity, Entity refEntity ,EntityGroup eg,Trans trans,Transaction transaction,PropToTransaction propToTransaction)
	{
		long commissionID = 0l;
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
						+ "~ Getting franchise entity group");
		Commission commission = new Commission();
		
		if(entity.getTypeID() == 2)
		{
			commission.setCommtarget("initiator");
		}
		else if(entity.getTypeID() == 3)
		{
			commission.setCommtarget("initiator_parent");
		}
		else if(entity.getTypeID() == 9)
		{
			if(entity.getId() == refEntity.getId())
				commission.setCommtarget("initiator_parent");
			else
				commission.setCommtarget("initiator_franchise");
		}
		
		if(pc.getChannelCommissionAmount().equalsIgnoreCase("0") || pc.getChannelCommissionAmount() == null)
			commission.setCommAmount("0");
		else
		{
			commission.setCommAmount(String.valueOf(getCommissionGroupWise(entity,eg,pc.getChannelCommissionAmount(),trans,transaction,propToTransaction)));
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
							+ "~ setting commission amount "+commission.getCommAmount());
		}
		
		if(pc.getChannelCommissionPercentage().equalsIgnoreCase("0") || pc.getChannelCommissionPercentage() == null)
			commission.setCommPercentage("0");
		else
		{
			commission.setCommPercentage(String.valueOf(getCommissionGroupWise(entity,eg,pc.getChannelCommissionPercentage(),trans,transaction,propToTransaction)));
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
							+ "~ setting commission percentage "+commission.getCommAmount());
		}
		
		commission.setTransTypeID(trans.getTransTypeID());
		commission.setChannelID(2L);
		commission.setCircleID(0L);
		commission.setGroupID(eg.getId());
		commission.setTransAccountTypeID(1L);
		commission.setTargetAccountTypeID(1L);
		commission.setMethod("commission");
		commission.setFromAmount("1");
		commission.setToAmount("99999999");
		commission.setCommSource("");
		
		try {
			commissionID = save(commission, transaction, propToTransaction);
		} catch (SQLException e) {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
							+ "~ SQlException in generating commission entry for entityID "+entity.getId()+" .Exception : "+e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
					+ "~ Exception in generating commission entry for entityID "+entity.getId()+" .Exception : "+e.toString());
			e.printStackTrace();
		}
		
		return commissionID;
	}
	
	public List<Commission> rectifyCommission(List<Commission> commissions, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction)
	{
		List<Commission> internalCommissions = new ArrayList<>();
		Commission rdCommission =null;
		Commission retailerCommission = null;
		for (Commission commission : commissions) {
			if(commission.getCommtarget().equalsIgnoreCase("initiator_franchise"))
			{
				internalCommissions.add(commission);
			}
			else if(commission.getCommtarget().equalsIgnoreCase("initiator"))
			{
				internalCommissions.add(commission);
				retailerCommission = commission;
			}
			else
			{
				rdCommission = commission;
			}
				
		}
		
		if(rdCommission != null)
		{
			if(!rdCommission.getCommAmount().equalsIgnoreCase("0"))
			{
				double comAmt = Double.parseDouble(rdCommission.getCommAmount()) - Double.parseDouble(retailerCommission.getCommAmount());
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("WL"), "info",
						transaction.getTransId() + "~" + transaction.getThreadName() + "~ RD commission amount changed from "+rdCommission.getCommAmount()+" to "+comAmt);
				rdCommission.setCommAmount(String.valueOf(comAmt));
			}
			
			if(!rdCommission.getCommPercentage().equalsIgnoreCase("0"))
			{
				double comPer = Double.parseDouble(rdCommission.getCommPercentage()) - Double.parseDouble(retailerCommission.getCommPercentage());
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("WL"), "info",
						transaction.getTransId() + "~" + transaction.getThreadName() + "~ RD commission percentage changed from "+rdCommission.getCommPercentage()+" to "+comPer);
				rdCommission.setCommPercentage(String.valueOf(comPer));
			}
		}
		
		internalCommissions.add(rdCommission);
		return internalCommissions;
		
	}
	
	/*
	 * Start Commission for new concept
	 */
	
	/*
	 * Formula : Commission ==> entityID,transTypeID,productID,amountRange
	 * 
	 * 1) Check and get group of retailer(/and RD) where entity != owner(done) 
	 * 2) If not or partial missing , check Product Commission(done) 
	 * 3) Check Commission table initiator -> find commission -> get
	 * parent commission -> calculate and insert commission
	 * 
	 * 4) Pay Initiator 5) Pay Initiator_ Parent 6) Pay Initiator_Franchise
	 * (If applicable)
	 */

	public List<Commission> getCommissionEntry(Entity entity, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {		
		Entity parentEntity = null;
		Entity franchiseEntity = null;
		EntityGroup entityGroup = null;
		EntityGroupMapping entityGroupMapping = null;
		ProductCommission productCommission = null;
		List<EntityGroupMapping> entityGroupMappings = null;
		Commission commission = null;
		List<Commission> commissions = new ArrayList<>();
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				Logger.getLogger("WL"), "info",
				transaction.getTransId() + "~" + transaction.getThreadName() + "~Entering generate commission module");

		productCommission = getProductCommission(entity, transaction,
				trans, propToTransaction);
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				Logger.getLogger("WL"), "debug",
				transaction.getTransId() + "~" + transaction.getThreadName() + "~ checking product commission");

		if (productCommission != null) {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "debug",
					transaction.getTransId() + "~" + transaction.getThreadName() + "~ product commission available");

			TransactionDAO transactionDAO = new TransactionDAOImpl();
			parentEntity = transactionDAO.getEntity(entity.getParentID());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
							+ "~getting parent Entity TypeID " + parentEntity.getTypeID());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
							+ "~Checking mapping of group for entity " + entity.getId());
			
			entityGroupMappings = getEntityGroupMappingList(entity,
					parentEntity, transaction, trans, propToTransaction);

			if (entityGroupMappings != null) {
				long egmID = 0;
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
								+ "~ Group count for commission except franchise " + entityGroupMappings.size());
				if(parentEntity.getTypeID() == 3 && entityGroupMappings.size() ==1)
				{
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
									+ "~ Retailer has not mapped for commission " + entityGroupMappings.size());
					
					entityGroupMapping = entityGroupMappings.get(0);
					if(entityGroupMapping.getEntityID() == entity.getParentID())
					{
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ getting entity group for retailer by RD ");
						
						entityGroup = getEntityGroup(entity,"Bronze","Retailer", transaction, trans, propToTransaction);
						
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ Getting Group info of Default Bronze of owner " + entity.getParentID());
						EntityGroupMapping mapping = new EntityGroupMapping(null, entity.getId(), entityGroup.getId());
						
						try {
							egmID = save(mapping, transaction, propToTransaction);
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ Generating default Bronze of 3 tier group mapping for retailer " + entity.getId());
						} catch (SQLException e) {
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ SQL exception occured " + e.toString());
							e.printStackTrace();
						} catch (Exception e) {
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ exception occured " + e.toString());
						}
						
						mapping.setId(egmID);
						
						entityGroupMappings.add(mapping);
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ creating model of Entity Group Mapping of retailer " + entity.getId() +" and adding to egm list");
							
					}
					else
					{
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ Wrongly mapped group, Retailer has group but RD not");
						return commissions;
					}
				}
				
				// checking commission 
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
								+ "~ Checking commission for the mapped group for entityID "+ entity.getId()+"and entityParentID " + entity.getParentID());
				EntityGroup groupFranchise = new EntityGroup();
				for (EntityGroupMapping groupMapping : entityGroupMappings) {
					if(parentEntity.getTypeID() == 3 && groupMapping.getEntityID() == parentEntity.getId())
					{
						groupFranchise.setId(groupMapping.getGroupID());
						commission = getCommissionInfo(parentEntity, groupMapping, transaction, trans, propToTransaction);
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ getting commission entry for RD entityParentID " + entity.getParentID());
						if(commission != null)
						{
							commissions.add(commission);
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ RD commission available and added to commission list for " + entity.getParentID());
						}
						else{
							EntityGroup	groupRD = getEntityGroup("select * from EntityGroup where id="+groupMapping.getGroupID(), transaction, trans, propToTransaction);
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ Getting RD entity group");
						
							long rdCommissionID = setAndCreateCommission(productCommission, parentEntity, parentEntity, groupRD, trans, transaction, propToTransaction);
							
							if(rdCommissionID!=0)
							{
								ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
										Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
												+ "~ RD commission has been entered, commissionID : " + rdCommissionID);
								
								commission = getCommissionInfo(parentEntity, groupMapping, transaction, trans, propToTransaction);
								commissions.add(commission);
								
								ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
										Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
												+ "~ RD commission available and added to list " + entity.getId());
							}

						}
					}
					else
					{
						if(groupFranchise.getId() <= 0)
						{
							groupFranchise.setId(groupMapping.getGroupID());
						}
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ getting commission entry for retailer entityID " + entity.getId());
						commission = getCommissionInfo(entity, groupMapping, transaction, trans, propToTransaction);
						if(commission != null)
						{
							commissions.add(commission);
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ Retailer commission available " + entity.getId());
						}
						else
						{
							EntityGroup	eg = getEntityGroup("select * from EntityGroup where id="+groupMapping.getGroupID(), transaction, trans, propToTransaction);
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ Getting entity group for retailer ");
						
							long CommissionID = setAndCreateCommission(productCommission, entity, parentEntity, eg, trans, transaction, propToTransaction);
							
							if(CommissionID!=0)
							{
								ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
										Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
												+ "~ retailer commission has been entered, commissionID : " + CommissionID);
								
								commission = getCommissionInfo(entity, groupMapping, transaction, trans, propToTransaction);
								commissions.add(commission);
								
								ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
										Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
												+ "~ retailer commission available and added to list " + entity.getId());
							}

						}
						
					}
				}
				
				
				// Adding Franchise commission entry to the commission list
				if(parentEntity.getTypeID() == 3)
				{
					commission = getCommissionInfo(parentEntity, groupFranchise, transaction, trans, propToTransaction);
				}
				else
				{
					commission = getCommissionInfo(entity, groupFranchise, transaction, trans, propToTransaction);
				}
				
				if(commission != null)
				{
					commissions.add(commission);
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
									+ "~ Franchise commission available " + entity.getId());
				}
				else
				{
					groupFranchise = getEntityGroup("select * from EntityGroup where id="+groupFranchise.getId(), transaction, trans, propToTransaction);
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
									+ "~ Getting franchise entity group");
					
					franchiseEntity = transactionDAO.getEntity(entity.getFranchise().substring(entity.getFranchise().length() - 10, entity.getFranchise().length()));
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
									+ "~ Getting franchise entity Info "+franchiseEntity.getId());
					if(franchiseEntity !=null)
					{
						ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
										+ "~ request for creating commission entry for franchise with id"+franchiseEntity.getId());
						long fCommissionID = setAndCreateCommission(productCommission, franchiseEntity, parentEntity, groupFranchise, trans, transaction, propToTransaction);
						
						if(fCommissionID!=0)
						{
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ Franchise commission has been entered, commissionID : " + fCommissionID);
							
							commission = getCommissionInfo(entity, groupFranchise, transaction, trans, propToTransaction);
							commissions.add(commission);
							
							ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
									Logger.getLogger("WL"), "debug", transaction.getTransId() + "~" + transaction.getThreadName()
											+ "~ Franchise commission available and added to list " + entity.getId());
						}
					}
				}
				// Get Commission list individually
				
			} else {
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("WL"), "info", transaction.getTransId() + "~" + transaction.getThreadName()
								+ "~ Group not available to map of " + entity.getId());
			}
		}
		else
		{
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("WL"), "info",
					transaction.getTransId() + "~" + transaction.getThreadName() + "~ product commission not available");
		}
		
		if(parentEntity.getTypeID() == 3 && commissions.size() > 2)
		{
			commissions =  rectifyCommission(commissions, transaction, trans, propToTransaction);
		}
		
		return commissions;
	}

	/*
	 * End Commission for new concept
	 */

	public List<Commission> getCommissionDetails(Entity entity, Transaction transaction, Trans trans, Accounts accounts,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("QL" + CommissionDAOService.class);
		commissionList = new ArrayList<>();

		StringBuilder queryBuilder = new StringBuilder();

		if (trans.getProductID() != null && trans.getProductID() != "0" && trans.getProductID() != "") {
			strQuery = "select c.*,ed.pan,ed.gstn from Commission c,Entity e,EntityData ed where c.method='commission' and c.transTypeID='"
					+ trans.getTransTypeID() + "' and c.productID='" + trans.getProductID()
					+ "' and c.groupID in (select groupID from EntityGroupMapping where entityID='" + entity.getId()
					+ "') and ed.entityID=e.parentID and e.id='" + entity.getId() + "' and " + trans.getAmount()
					+ " between c.fromAmount and c.toAmount ";
		} else {
			strQuery = "select c.*,ed.pan,ed.gstn from Commission c,Entity e,EntityData ed where c.method='commission' and c.transTypeID='"
					+ trans.getTransTypeID()
					+ "' and c.groupID in (select groupID from EntityGroupMapping where entityID='" + entity.getId()
					+ "') and ed.entityID=e.parentID and e.id='" + entity.getId() + "' and " + trans.getAmount()
					+ " between c.fromAmount and c.toAmount ";
		}

		queryBuilder.append(strQuery);
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			con = DBPool.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				Commission commission = new Commission();
				commission.setId(rs.getLong("id"));
				commission.setTransTypeID(rs.getLong("transTypeID"));
				commission.setChannelID(rs.getLong("channelID"));
				commission.setCircleID(rs.getLong("circleID"));
				commission.setGroupID(rs.getLong("groupID"));
				commission.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				commission.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				commission.setMethod(rs.getString("method"));
				commission.setFromAmount(rs.getString("fromAmount"));
				commission.setToAmount(rs.getString("toAmount"));
				commission.setCommAmount(rs.getString("commAmount"));
				commission.setCommPercentage(rs.getString("commPercentage"));
				commission.setCommSource(rs.getString("commSource"));
				commission.setCommtarget(rs.getString("commtarget"));
				commission.setPan(rs.getString("pan"));
				commission.setGstn(rs.getString("gstn"));
				commissionList.add(commission);
			}
		} catch (Exception e) {
			commissionList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return commissionList;

	}

	public double commissionDeduction(Commission commission, Entity entity, Transaction transaction, Trans trans1,
			PropToTransaction propToTransaction) {

		/*
		 * 1) Check method for margin / commission If method is margin then get
		 * the commAmount(margin) and commPercentage(margin) and deduct the
		 * margin from the voucher amount
		 * 
		 * 2)Otherwise Check the commission percentage to get amount as per
		 * commission and amount too
		 * 
		 */

		Trans trans = new Trans(trans1);
		// Checking method for margin / commission Start

		if (commission.getMethod() != null) {

			if (commission.getMethod().equalsIgnoreCase("margin")) {

				// returning remaining amount after deducting the margin

				return manageMargin(commission, transaction, trans, propToTransaction);
			} else if (commission.getMethod().equalsIgnoreCase("commission")) {
				return manageCommissionWithoutGSTN(commission, entity, transaction, trans, propToTransaction);

			}
		}

		/*
		 * End Checking method for margin / commission
		 */

		return 0.0;

	}

	public long getChildTransaction(Trans trans, String commAmt, Commission commission, Transaction transaction,
			PropToTransaction propToTransaction) {
		Trans commissionTrans = new Trans("0", 0, new Date(), new Date(), commission.getCommSource(),
				commission.getCommtarget(), commAmt, "2", trans.getResultID(), trans.getId(), trans.getChannelID(),
				trans.getParam3(), trans.getIsAtomic());
		long commissionTransStatus = 0;
		try {

			commissionTransStatus = new TransactionDaoService().saveFeeTrans(commissionTrans, transaction,
					propToTransaction);
		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + CommissionDAOService.class), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")."
							+ transaction.getThreadName());
		}
		return commissionTransStatus;
	}

	public double manageCommissionWithoutGSTN(Commission commission, Entity entity, Transaction transaction,
			Trans trans1, PropToTransaction propToTransaction) {
		/*
		 * 1) Method is commission then getting the commAmount and
		 * commPercentage and deduct the commission from the actual amount
		 * 
		 */
		Trans trans = new Trans(trans1);

		double commissionAmt = 0.0;
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "info", transaction.getTransId() + " ~I: Manage commission. initiator("
						+ transaction.getInitiator() + "). " + transaction.getThreadName());

		// Getting the commAmount
		if (commission.getCommAmount().length() > 0 && isDouble(commission.getCommAmount())) {
			commissionAmt = Double.parseDouble(commission.getCommAmount());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Commissoin Amount Rs." + commission.getCommAmount()
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
		} else {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Commissoin Amount not available"
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
		}

		// Getting the commPercentage
		if (commission.getCommPercentage().length() > 0 && isDouble(commission.getCommPercentage())) {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Commissoin percentage " + commission.getCommPercentage() + "%"
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: getting Commissoin amout from Commissoin percentage("
							+ commission.getCommPercentage() + "%) and adding to total Commissoin amount . initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());

			// Calculating total Commission
			commissionAmt += getAmountOnMarginPercentage(Double.parseDouble(trans.getAmount()),
					Double.parseDouble(commission.getCommPercentage()));

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Total amount Rs.(" + trans.getAmount()
							+ ") and Total Commission amount Rs." + commissionAmt + ". initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());
		} else {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Commission percentage not available"
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
		}

		// Creating Child transaction for commission withoout gstn

		commissionAmt = (double) Math.round(commissionAmt * 10000) / 10000;
		long commTransID = 0;

		if (commission.getCommtarget().equals("initiator")) {
			BasicTransCheck basicTransCheck = new BasicTransCheck();
			trans.setParam3(String.format("%.4f",
					basicTransCheck.checkBalance(entity.getId().toString(), transaction, propToTransaction)));

			commission.setCommtarget(entity.getId().toString());

			commTransID = getChildTransaction(trans, String.valueOf(commissionAmt),
					commission, transaction, propToTransaction);
			try {
				new AccountsDAOService().creditTargetAccount(entity.getId(), commissionAmt, trans.getId(), transaction,
						propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~I: initiator commission-" + commissionAmt
								+ " .credit sucessfully from " + entity.getId());
			} catch (Exception e) {

				e.printStackTrace();
			}

		} else if (commission.getCommtarget().equals("initiator_parent")) {
			BasicTransCheck basicTransCheck = new BasicTransCheck();
			trans.setParam3(String.format("%.4f", basicTransCheck.checkBalance(String.valueOf(entity.getParentID()),
					transaction, propToTransaction)));

			commission.setCommtarget(String.valueOf(entity.getParentID()));

			commTransID = getChildTransaction(trans, String.valueOf(commissionAmt),
					commission, transaction, propToTransaction);
			try {
				new AccountsDAOService().creditTargetAccount(entity.getParentID(), commissionAmt, trans.getId(),
						transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~I: initiator_parent commission-"
								+ commissionAmt + " .credit sucessfully from " + entity.getParentID());
			} catch (Exception e) {

				e.printStackTrace();
			}

		} else if (commission.getCommtarget().equals("initiator_franchise")) {

			try {
				TransactionDAO transactionDAO = new TransactionDAOImpl();
				long id = transactionDAO
						.getRowID("select parentID as id from Entity where id='" + entity.getParentID() + "'");
				BasicTransCheck basicTransCheck = new BasicTransCheck();
				trans.setParam3(String.format("%.4f",
						basicTransCheck.checkBalance(String.valueOf(id), transaction, propToTransaction)));

				commission.setCommtarget(String.valueOf(id));

				commTransID = getChildTransaction(trans, String.valueOf(commissionAmt),
						commission, transaction, propToTransaction);
				new AccountsDAOService().creditTargetAccount(id, commissionAmt, trans.getId(), transaction,
						propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~I: initiator_franchise commission-"
								+ commissionAmt + " .credit sucessfully from " + id);
			} catch (Exception e) {

				e.printStackTrace();
			}

		}

		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "info",
				transaction.getTransId() + " ~I: Clild Transaction for commission. Commission Transaction ID"
						+ commTransID + " , Parent Transaction ID : " + trans.getId() + ". initiator("
						+ transaction.getInitiator() + "). " + transaction.getThreadName());
		double remAmount = Double.parseDouble(trans.getAmount()) - commissionAmt;
		return (double) Math.round(remAmount * 10000) / 10000;
	}

	public double manageGST(double comm, Commission commission, Entity entity, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		/*
		 * 1) Method is commission then getting the commAmount and
		 * commPercentage and deduct the commission from the actual amount
		 * 
		 */
		SystemConfigDAOService configDAOService = new SystemConfigDAOService();

		double gstAmt = 0.0, remAmount = 0.0, tdsAmt = 0.0;

		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "info", transaction.getTransId() + " ~I: Manage GSTN. initiator("
						+ transaction.getInitiator() + "). " + transaction.getThreadName());
		if (commission.getGstn() != null && commission.getGstn() != "") {
			String gstn = configDAOService.getSystemConfigFromList(propToTransaction.getSystemConfigs(), "gst");

			// Calculating total GSTN
			gstAmt += getAmountOnMarginPercentage(comm, Double.parseDouble(gstn));

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Total amount Rs.(" + comm + ") and Total GST amount Rs." + gstAmt
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			gstAmt = (double) Math.round(gstAmt * 10000) / 10000;
			try {
				new TransactionDaoService().saveTransData(trans.getId(), "gst", String.valueOf(gstAmt), transaction,
						propToTransaction);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		// Calculate total TDS with PAN

		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "info", transaction.getTransId() + " ~I: Manage TDS. initiator(" + transaction.getInitiator()
						+ "). " + transaction.getThreadName());
		if (commission.getPan() != null && commission.getPan() != "") {
			String tds = configDAOService.getSystemConfigFromList(propToTransaction.getSystemConfigs(), "pan");

			// Calculating total TDS Amount
			tdsAmt += getAmountOnMarginPercentage(comm, Double.parseDouble(tds));

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Total amount Rs.(" + comm + ") and Total TDS amount with PAN Rs."
							+ tdsAmt + ". initiator(" + transaction.getInitiator() + "). "
							+ transaction.getThreadName());

			tdsAmt = (double) Math.round(tdsAmt * 10000) / 10000;

			try {
				new TransactionDaoService().saveTransData(trans.getId(), "tds", String.valueOf(tdsAmt), transaction,
						propToTransaction);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			String tds = configDAOService.getSystemConfigFromList(propToTransaction.getSystemConfigs(), "non_pan");

			// Calculating total TDS Amount
			tdsAmt += getAmountOnMarginPercentage(comm, Double.parseDouble(tds));

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Total amount Rs.(" + comm + ") and Total TDS amount without Rs."
							+ tdsAmt + ". initiator(" + transaction.getInitiator() + "). "
							+ transaction.getThreadName());

			// double remAmount = Double.parseDouble(trans.getAmount()) -
			// commissionAmt;

			// Creating Child transaction for commission withoout gstn
			tdsAmt = (double) Math.round(tdsAmt * 10000) / 10000;

			try {
				new TransactionDaoService().saveTransData(trans.getId(), "tds", String.valueOf(tdsAmt), transaction,
						propToTransaction);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		TransactionDAO transactionDAO = new TransactionDAOImpl();

		long id = transactionDAO.getRowID("select id from Entity where msisdn='"
				+ entity.getFranchise().substring(entity.getFranchise().length() - 10, entity.getFranchise().length())
				+ "'");
		try {
			new AccountsDAOService().creditTargetAccount(id, gstAmt, trans.getId(), transaction, propToTransaction);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I:initiator_franchise~" + entity.getFranchise()
							+ "~GST-" + gstAmt + " .credit sucessfully from " + id);
		} catch (Exception e) {

			e.printStackTrace();
		}
		try {
			new AccountsDAOService().debitTargetAccount(id, tdsAmt, trans.getId(), transaction, propToTransaction);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: initiator_franchise~" + entity.getFranchise()
							+ "~TDS -" + tdsAmt + " .debit sucessfully from " + id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		remAmount = gstAmt - tdsAmt;

		return (double) Math.round(remAmount * 10000) / 10000;
	}
	public double manageMargin(Commission commission,Transaction transaction,Trans trans,double amount,PropToTransaction propToTransaction)
	{
		/*
		 * 1) Method is margin then getting
		 * the commAmount(margin) and commPercentage(margin) and deduct the
		 * margin from the actual amount
		 * 
		 */
		double marginAmt=0.0;
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Manage margin. initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());
			
			//Getting the commAmount(margin)
			if(commission.getCommAmount().length()>0 && isDouble(commission.getCommAmount()))
			{
				marginAmt = Double.parseDouble(commission.getCommAmount());
				
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info",
						transaction.getTransId() + " ~I: Margin Amount Rs." + commission.getCommAmount()
								+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			}
			else
			{
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info",
						transaction.getTransId() + " ~I: Margin Amount not available"
								+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			}
			
			//Getting the commPercentage(margin)
			if(commission.getCommPercentage().length() > 0 && isDouble(commission.getCommPercentage()))
			{
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info",
						transaction.getTransId() + " ~I: Margin percentage " + commission.getCommPercentage()+"%"
								+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
				
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info",
						transaction.getTransId() + " ~I: getting margin amout from margin percentage("+commission.getCommPercentage()+"%) and adding to total margin amount . initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
				
				// Calculating total margin
				marginAmt += getAmountOnMarginPercentage(amount,Double.parseDouble(commission.getCommPercentage()));
				
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info",
						transaction.getTransId() + " ~I: Total amount Rs.("+trans.getAmount()+") and Total margin amount Rs." + marginAmt
								+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			}
			else
			{
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info",
						transaction.getTransId() + " ~I: Margin percentage not available"
								+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
			}
			marginAmt=	(double) Math.round(marginAmt * 10000) / 10000;
		try {
			new  TransactionDaoService().saveTransData(trans.getId(),"margin",String.valueOf(marginAmt), transaction, propToTransaction);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (amount-marginAmt);
	}
	public double manageMargin(Commission commission, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {
		/*
		 * 1) Method is margin then getting the commAmount(margin) and
		 * commPercentage(margin) and deduct the margin from the actual amount
		 * 
		 */
		double marginAmt = 0.0;
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "info", transaction.getTransId() + " ~I: Manage margin. initiator("
						+ transaction.getInitiator() + "). " + transaction.getThreadName());

		// Getting the commAmount(margin)
		if (commission.getCommAmount().length() > 0 && isDouble(commission.getCommAmount())) {
			marginAmt = Double.parseDouble(commission.getCommAmount());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Margin Amount Rs." + commission.getCommAmount()
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
		} else {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Margin Amount not available" + ". initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());
		}

		// Getting the commPercentage(margin)
		if (commission.getCommPercentage().length() > 0 && isDouble(commission.getCommPercentage())) {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Margin percentage " + commission.getCommPercentage() + "%"
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: getting margin amout from margin percentage("
							+ commission.getCommPercentage() + "%) and adding to total margin amount . initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());

			// Calculating total margin
			marginAmt += getAmountOnMarginPercentage(Double.parseDouble(trans.getAmount()),
					Double.parseDouble(commission.getCommPercentage()));

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Total amount Rs.(" + trans.getAmount()
							+ ") and Total margin amount Rs." + marginAmt + ". initiator(" + transaction.getInitiator()
							+ "). " + transaction.getThreadName());
		} else {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Margin percentage not available"
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
		}
		try {
			new TransactionDaoService().saveTransData(trans.getId(), "margin", String.valueOf(marginAmt), transaction,
					propToTransaction);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (Double.parseDouble(trans.getAmount()) - marginAmt);
	}

	public String prepareMSG(PropToTransaction propToTransaction, String transID, String key, String defMessage) {
		String strMSG = propertyToMap.getPropertyValue(propToTransaction.getPropToMap(), key);
		if (strMSG != null) {
			strMSG = strMSG + " Transaction ID: " + transID;
		} else {
			strMSG = defMessage + ",Transaction ID: " + transID;
		}

		return strMSG;
	}

	public boolean isInteger(String str) {
		int size = str.length();

		for (int i = 0; i < size; i++) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}

		return size > 0;
	}

	public boolean isDouble(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public double getAmountOnMarginPercentage(double amount, double percentage) {
		return (double) (amount * percentage) / 100;
	}

	public static void main(String args[]) {
		CommissionDAOService service = new CommissionDAOService();
		double marginAmt = 0.0;

		marginAmt += service.getAmountOnMarginPercentage(Double.parseDouble("10"), Double.parseDouble("1.03"));
		System.out.println("margin amt : " + marginAmt);
		System.out.println(String.format("%.3f", 2.5));
	}

	public double marginamount(double amt, double margin) {
		return (amt - margin);
	}

}
