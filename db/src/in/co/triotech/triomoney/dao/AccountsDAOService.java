package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Accounts;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;

public class AccountsDAOService {
	private TransactionDAO transactionDAO = null;
	private Accounts accounts = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	String type = null;
	Logger accessLog = Logger.getLogger("QL" + AccountsDAOService.class);
	Logger excepLog = Logger.getLogger("EL" + AccountsDAOService.class);

	// con = new ConnectionPool().getConnection();
	// stm = con.createStatement();
	public boolean updateAccountAvailableBalance(Accounts accounts, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();

			transactionDAO = new TransactionDAOImpl();
			isUpdate = transactionDAO.update(conn,
					"update Accounts set availableBalance=?,currentBalance=? where entityID = ?", new Object[] {
							accounts.getAvailableBalance(), accounts.getCurrentBalance(), accounts.getEntityID() },
					transaction, propToTransaction);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug",
					transaction.getTransId() + "~D~" + "update Accounts set availableBalance="
							+ accounts.getAvailableBalance() + ",currentBalance=" + accounts.getCurrentBalance()
							+ " where entityID=" + accounts.getEntityID() + ". initiator(" + transaction.getInitiator()
							+ ") " + transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return isUpdate;
	}
	
	public boolean reverseDebtorCreditorOnFailed(Trans trans, double margin, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		double amount = 0.0;
		amount  = Double.parseDouble(trans.getAmount())-margin;
		Accounts accounts = null;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
			String strQuery = "";
			if(Integer.parseInt(trans.getDebtor())!=0){
				accounts = getAccountsByEntityID(Long.parseLong(trans.getDebtor()), transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "info",
						transaction.getTransId() + " ~ D: " + "Before account update account available balance :"+String.valueOf(accounts.getAvailableBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "debug",
						transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
			strQuery = "update Accounts set availableBalance=(availableBalance+" + amount + ")  where entityID='"
					+ trans.getDebtor() + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			accounts = getAccountsByEntityID(Long.parseLong(trans.getDebtor()), transaction, propToTransaction);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "info",
					transaction.getTransId() + " ~ D: " + "After account update account available balance :"+String.valueOf(accounts.getAvailableBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
			}
			if(Integer.parseInt(trans.getCreditor())!=0){
				accounts = getAccountsByEntityID(Long.parseLong(trans.getCreditor()), transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "info",
						transaction.getTransId() + " ~ D: " + "Before account update account available balance :"+String.valueOf(accounts.getAvailableBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "debug",
						transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				strQuery = "update Accounts set availableBalance=(availableBalance-" + amount + ")  where entityID='"
						+trans.getCreditor() + "'";
				isUpdate = transactionDAO.updateById(strQuery);
				accounts = getAccountsByEntityID(Long.parseLong(trans.getCreditor()), transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "info",
						transaction.getTransId() + " ~ D: " + "After account update account available balance :"+String.valueOf(accounts.getAvailableBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				}
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	public Accounts getAccountsByEntityIDAndMSISDN(Long entityID, Long msisdn, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException {
		String strQuery = "select a.* from Accounts a , Entity e where e.id=a.entityID and e.msisdn="+msisdn+"  limit 1 ";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ D: " + strQuery + ". initiator("
							+ transaction.getInitiator() + ") " + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection(); // dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				accounts = new Accounts(rs.getLong("id"), rs.getLong("entityID"), rs.getDouble("currentBalance"),
						rs.getDouble("availableBalance"), rs.getString("lowerLimit"), rs.getString("upperLimit"),
						rs.getLong("typeID"));
			}
		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return accounts;
	}
	public boolean debit_reserve(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set availableBalance=(availableBalance-" + amount + ") where entityID='"
					+ entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + "~D~" + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
			
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	
	public boolean debitAccounts(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance-" + amount + ") where entityID='"
					+ entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}


	public boolean debit_reverse(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set availableBalance=(availableBalance+" + amount + ") where entityID='"
					+ entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	
	
	public boolean credit_reserve(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		long id = 0;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
				Connection conn = null;
				try {
					conn = DBPool.getConnection();// dataSource.getConnection();
				} catch (Exception e) {
					StackTraceElement l = e.getStackTrace()[0];
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/"
									+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
									+ transaction.getInitiator() + ")" + transaction.getThreadName());
				}
				if (conn != null) {
					id = transactionDAO.save(conn,
							"insert into special_wallet(created_date,entityID,amount,type,transID,param1) values(?,?,?,?,?,?)",
							new Object[] { new Date(), entityID, "+" + amount, "C", transid,"currentBalance"}, transaction,
							propToTransaction);
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							excepLog, "debug",
							transaction.getTransId() + " ~D~" + "insert into special_wallet(created_date,entityID,amount,type,transID,param1) values(?,?,?,?,?,?)"+ " \n.initiator(" + transaction.getInitiator() + ")"
									+ transaction.getThreadName());
				}
				if (id != 0) {
					isUpdate = true;
				}
			
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	public boolean creditAccounts(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		long id = 0;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
				Connection conn = null;
				try {
					conn = DBPool.getConnection();// dataSource.getConnection();
				} catch (Exception e) {
					StackTraceElement l = e.getStackTrace()[0];
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/"
									+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
									+ transaction.getInitiator() + ")" + transaction.getThreadName());
				}
				if (conn != null) {
					id = transactionDAO.save(conn,
							"insert into special_wallet(created_date,entityID,amount,type,transID,param1) values(?,?,?,?,?,?)",
							new Object[] { new Date(), entityID, "+" + amount, "C", transid,"availableBalance"}, transaction,
							propToTransaction);
				}
				if (id != 0) {
					isUpdate = true;
				}
			
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	public boolean credit_reverse(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		long id = 0;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
				Connection conn = null;
				try {
					conn = DBPool.getConnection();// dataSource.getConnection();
				} catch (Exception e) {
					StackTraceElement l = e.getStackTrace()[0];
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/"
									+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
									+ transaction.getInitiator() + ")" + transaction.getThreadName());
				}
				if (conn != null) {
					id = transactionDAO.save(conn,
							"insert into special_wallet(created_date,entityID,amount,type,transID,param1) values(?,?,?,?,?,?)",
							new Object[] { new Date(), entityID, "-" + amount, "D", transid,"currentBalance"}, transaction,
							propToTransaction);
				}
				if (id != 0) {
					isUpdate = true;
				}
			
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}


	public boolean debitTargetAccount(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance-" + amount + "),availableBalance=(availableBalance-" + amount + ") where entityID='"
					+ entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	public boolean creditTargetAccount(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance+" + amount + "),availableBalance=(availableBalance+" + amount + ") where entityID='"
					+ entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	public Accounts getAccountsByEntityID(Long entityID, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException {
		String strQuery = "select * from Accounts a where a.entityID ='" + entityID + "'";
		transactionDAO = new TransactionDAOImpl();

		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ D: " + strQuery + ". initiator("
							+ transaction.getInitiator() + ") " + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection(); // dataSource.getConnection();//new
											// ConnectionPool().getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				accounts = new Accounts(rs.getLong("id"), rs.getLong("entityID"), rs.getDouble("currentBalance"),
						rs.getDouble("availableBalance"), rs.getString("lowerLimit"), rs.getString("upperLimit"),
						rs.getLong("typeID"));
			}
		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")");

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return accounts;
	}

	public Accounts getAccountsByEntityIDAndTypeID(Long entityID, Long typeID, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException {
		String strQuery = "select a.* from Accounts a where a.entityID ='" + entityID + "' and a.typeID='" + typeID
				+ "'  limit 1 ";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ D: " + strQuery + ". initiator("
							+ transaction.getInitiator() + ") " + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection(); // dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				accounts = new Accounts(rs.getLong("id"), rs.getLong("entityID"), rs.getDouble("currentBalance"),
						rs.getDouble("availableBalance"), rs.getString("lowerLimit"), rs.getString("upperLimit"),
						rs.getLong("typeID"));
			}
		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return accounts;
	}
	public boolean reverseDebtorCreditorOnSuccess(Trans trans, double margin, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		double amount = 0.0;
		amount  = Double.parseDouble(trans.getAmount())-margin;
		try {
			Accounts accounts = null;
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
			String strQuery = "";
			if(Integer.parseInt(trans.getDebtor())!=0){
				accounts = getAccountsByEntityID(Long.parseLong(trans.getDebtor()), transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "info",
						transaction.getTransId() + " ~ D: " + "Before account update account current balance :"+String.valueOf(accounts.getCurrentBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "debug",
						transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
			strQuery = "update Accounts set currentBalance=(currentBalance-" + amount + ")  where entityID='"
					+ trans.getDebtor() + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			accounts = getAccountsByEntityID(Long.parseLong(trans.getDebtor()), transaction, propToTransaction);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "info",
					transaction.getTransId() + " ~ D: " + "After account update account current balance :"+String.valueOf(accounts.getCurrentBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
			}
			if(Integer.parseInt(trans.getCreditor())!=0){
				accounts = getAccountsByEntityID(Long.parseLong(trans.getCreditor()), transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "info",
						transaction.getTransId() + " ~ D: " + "Before account update account current balance :"+String.valueOf(accounts.getCurrentBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "debug",
						transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				strQuery = "update Accounts set currentBalance=(currentBalance+" + amount + ")  where entityID='"
						+trans.getCreditor() + "'";
				isUpdate = transactionDAO.updateById(strQuery);
				accounts = getAccountsByEntityID(Long.parseLong(trans.getCreditor()), transaction, propToTransaction);
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						excepLog, "info",
						transaction.getTransId() + " ~ D: " + "After account update account current balance :"+String.valueOf(accounts.getCurrentBalance())+", with entity id:"+accounts.getEntityID()+ " \n.initiator(" + transaction.getInitiator() + ")"
								+ transaction.getThreadName());
				}
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
	public boolean UpdateAmount(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException, Exception {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set availableBalance=(availableBalance+" + amount + ") ,currentBalance= (currentBalance+" + amount + ") where entityID='"
					+ entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug",
					transaction.getTransId() + " ~ D: " + strQuery.toString()+ " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}
}
