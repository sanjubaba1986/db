package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.connectivity.PropertyToMap;
import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Accounts;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.model.Fee;
import in.co.triotech.util.BasicFieldCheck;

public class FeeDAOService {
	PropertyToMap propertyToMap = new PropertyToMap();

	DataSource dataSource = null;// ConnectionPool.setUp();
	List<Fee> feeList = null;
	Logger accessLog = Logger.getLogger("WL" + FeeDAOService.class);
	BasicFieldCheck basicFieldCheck = new BasicFieldCheck();

	public List<Fee> getFeeDetails(Entity entity, Transaction transaction, Trans trans, Accounts accounts,
			PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("querylogger" + FeeDAOService.class);
		feeList = new ArrayList<>();
		StringBuilder queryBuilder = new StringBuilder();

		if (trans.getProductID() != null && trans.getProductID() != "0" && trans.getProductID() != "") {
			strQuery = "select f.* from Fee f  where f.transTypeID='" + trans.getTransTypeID() + "' and f.productID='"
					+ trans.getProductID()
					+ "' and f.groupID in (0,(select groupID from EntityGroupMapping where entityID='" + entity.getId()
					+ "')) and " + trans.getAmount() + " between f.fromAmount and f.toAmount ";
		} else {
			strQuery = "select f.* from Fee f  where f.transTypeID='" + trans.getTransTypeID()
					+ "' and f.groupID in (0,(select groupID from EntityGroupMapping where entityID='" + entity.getId()
					+ "')) and " + trans.getAmount() + " between f.fromAmount and f.toAmount ";
		}
		queryBuilder.append(strQuery);
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				Fee fee = new Fee();
				fee.setId(rs.getLong("id"));
				fee.setTransTypeID(rs.getLong("transTypeID"));
				fee.setChannelID(rs.getLong("channelID"));
				fee.setCircleID(rs.getLong("circleID"));
				fee.setGroupID(rs.getLong("groupID"));
				fee.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				fee.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				fee.setMethod(rs.getString("method"));
				fee.setFromAmount(rs.getString("fromAmount"));
				fee.setToAmount(rs.getString("toAmount"));
				fee.setFeeAmount(rs.getString("feeAmount"));
				fee.setFeePercentage(rs.getString("feePercentage"));
				fee.setFeeSource(rs.getString("feeSource"));
				fee.setFeeTarget(rs.getString("feeTarget"));
				fee.setProductID(rs.getInt("productID"));
				feeList.add(fee);
			}
		} catch (Exception e) {
			feeList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + FeeDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return feeList;

	}

	public List<Fee> getFeeDetails(long transTypeId, long circleID, long entityID, long acTypeID, String amount,
			Transaction transaction, PropToTransaction propToTransaction) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		String strQuery = null;
		Logger queryLog = Logger.getLogger("querylogger" + FeeDAOService.class);
		feeList = new ArrayList<>();

		StringBuilder queryBuilder = new StringBuilder();
		strQuery = "select f.* from Fee f where f.transTypeID ='" + transTypeId + "' and f.productID='"
				+ transaction.getProductID()
				+ "' and f.groupID in (0,(select egm.groupID from EntityGroupMapping egm where egm.entityID='"
				+ entityID + "'))  and f.transAccountTypeID = '" + acTypeID + "' and " + amount
				+ " between f.fromAmount and f.toAmount ";
		queryBuilder.append(strQuery);
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					queryLog, "debug", transaction.getTransId() + " ~D " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "Query",
			// "querylogger",
			// strQuery.toString(),LogConfiguration.writeLogPath()+"/Query_Logs/"+"/"+GetTimeStamp.getDate()+".log");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(strQuery);
			while (rs.next()) {
				Fee fee = new Fee();
				fee.setId(rs.getLong("id"));
				fee.setTransTypeID(rs.getLong("transTypeID"));
				fee.setChannelID(rs.getLong("channelID"));
				fee.setCircleID(rs.getLong("circleID"));
				fee.setGroupID(rs.getLong("groupID"));
				fee.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				fee.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				fee.setMethod(rs.getString("method"));
				fee.setFromAmount(rs.getString("fromAmount"));
				fee.setToAmount(rs.getString("toAmount"));
				fee.setFeeAmount(rs.getString("feeAmount"));
				fee.setFeePercentage(rs.getString("feePercentage"));
				fee.setFeeSource(rs.getString("feeSource"));
				fee.setFeeTarget(rs.getString("feeTarget"));
				feeList.add(fee);
				// fee = new
				// Fee(rs.getLong("id"),rs.getLong("transTypeID"),rs.getLong("channelID"),
				// rs.getLong("circleID"),rs.getLong("groupID"),rs.getLong("transAccountTypeID"),rs.getLong("targetAccountTypeID"),rs.getString("method"),
				// rs.getString("fromAmount"),rs.getString("toAmount"),
				// rs.getString("feeAmount"),rs.getString("feePercentage"),rs.getString("feeSource"),rs.getString("feeTarget"));
			}
		} catch (Exception e) {
			feeList = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StackTraceElement l = e.getStackTrace()[0];
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + FeeDAOService.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + l.getClassName() + "/"
								+ l.getMethodName() + ":" + l.getLineNumber() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			}
		}
		return feeList;

	}

	public double manageFee(Fee fee, Entity entity, Transaction transaction, Trans trans,
			PropToTransaction propToTransaction) {

		double feeAmt = 0.0;
		ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
				accessLog, "info", transaction.getTransId() + " ~I: Manage fee. initiator(" + transaction.getInitiator()
						+ "). " + transaction.getThreadName());

		// Getting the feeAmount
		if (fee.getFeeAmount().length() > 0 && isDouble(fee.getFeeAmount())) {
			feeAmt = Double.parseDouble(fee.getFeeAmount());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Fee Amount Rs." + fee.getFeeAmount()
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());
		} else {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Fee Amount not available" + ". initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());
		}

		// Getting the feePercentage
		if (fee.getFeePercentage().length() > 0 && isDouble(fee.getFeePercentage())) {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Fee percentage " + fee.getFeePercentage() + "%"
							+ ". initiator(" + transaction.getInitiator() + "). " + transaction.getThreadName());

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: getting fee amout from fee percentage(" + fee.getFeePercentage()
							+ "%) and adding to total fee amount . initiator(" + transaction.getInitiator() + "). "
							+ transaction.getThreadName());

			// Calculating total fee
			feeAmt += getAmountOnFeePercentage(Double.parseDouble(trans.getAmount()),
					Double.parseDouble(fee.getFeePercentage()));
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I: Total amount Rs.(" + trans.getAmount()
							+ ") and Total fee amount Rs." + feeAmt + ". initiator(" + transaction.getInitiator()
							+ "). " + transaction.getThreadName());
		} else {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Fee percentage not available" + ". initiator("
							+ transaction.getInitiator() + "). " + transaction.getThreadName());
		}
		/*
		 * Create Fee Trans
		 */
		feeAmt = (double) Math.round(feeAmt * 10000) / 10000;
		Trans feeTrans = new Trans("0", 35, String.valueOf(fee.getProductID()), new Date(), new Date(),
				entity.getId().toString(), fee.getFeeTarget(), String.valueOf(feeAmt), "2", trans.getResultID(),
				trans.getId(), trans.getChannelID(), trans.getIsAtomic());
		try {
			new TransactionDaoService().saveFeeTrans(feeTrans, transaction, propToTransaction);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return feeAmt;
	}

	public boolean isDouble(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public double getAmountOnFeePercentage(double amount, double percentage) {
		return (double) (amount * percentage) / 100;
	}

	public int getFeeAmountOnPercentage(int amount, int percent) {
		return (amount * percent) / 100;
	}

	public boolean isInteger(String str) {
		int size = str.length();

		for (int i = 0; i < size; i++) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}

		return size > 0;
	}

	public static void main(String[] args) {

		double value = 23;
		double rounded = (double) Math.round(value * 10000) / 10000;
		System.out.println(value + " rounded is " + rounded);
	}
}