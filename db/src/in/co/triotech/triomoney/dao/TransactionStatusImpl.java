package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;

import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;

public class TransactionStatusImpl implements TransactionStatus {
	
	Logger accessLog = Logger.getLogger("TL");
	@Override
	public void commit(String successMsg) {

	}

	@Override
	public void rollBack(String rollbackMsg, String ip, int port,String appName,Transaction transaction,PropToTransaction propToTransaction) {
		try {
			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", accessLog,"info",transaction.getTransId()+" ~Rollback: "+rollbackMsg+". initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
		}

	}

	@Override
	public void rollBack(String rollbackMsg,String appName,Transaction transaction,PropToTransaction propToTransaction) {
		try {
			ReadLog4J.writeLog("Worker_Logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log", accessLog,"info",transaction.getTransId()+" ~ Rollback:"+rollbackMsg+". initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());

		}
		
	}

}
