package in.co.triotech.triomoney.dao;

import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;

public interface TransactionStatus {
	
	void commit(String successMsg);
	
	void rollBack(String rollbackMsg,String ip,int port,String appName,Transaction transaction,PropToTransaction propToTransaction);
	void rollBack(String rollbackMsg,String appName,Transaction transaction,PropToTransaction propToTransaction);
}
