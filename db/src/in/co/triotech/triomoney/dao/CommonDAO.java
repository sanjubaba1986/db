package in.co.triotech.triomoney.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.GetActiveProduct;
import in.co.triotech.triomoney.GetTransactionStatus;
import in.co.triotech.triomoney.Register;
import in.co.triotech.triomoney.model.Commission;

/**
 * @author Abhay
 *
 */
public class CommonDAO {
	private Connection conn;
	private TransactionDAO transactionDAO = null;
	private Statement stm = null;
	private ResultSet rs = null;
	private Connection con = null;
	Logger accessLog = Logger.getLogger("QL" + CommonDAO.class);
	Logger excepLog = Logger.getLogger("EL" + CommonDAO.class);

	public long isWalletRegistered(String msisdn, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException {
		long entityId = 0;
		setStm(null);
		TransactionDAOImpl transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		Connection conn = null;
		try {
			conn = DBPool.getConnection();
		} catch (Exception e) {

		}
		if (conn != null) {
			try {
				setStm(conn.createStatement());
			} catch (SQLException e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
			}
			String query = "select e.id from Entity e,Accounts a where a.entityID=e.id and e.msisdn=" + msisdn
					+ " and e.statusID='1' ";
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info", transaction.getTransId() + " ~I: Query " + query + " initiator("
							+ transaction.getInitiator() + "). ");
			entityId = transactionDAO.getRowID(query);
		}

		return entityId;
	}

	public Trans getTransByTransID(long transID, Transaction transaction, PropToTransaction propToTransaction) {
		Trans trans = null;
		String strQuery = "select t.* from Transaction t where t.id='" + transID + "'  limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				trans = new Trans();
				trans.setId(rs.getLong("id"));
				trans.setInitiator(rs.getString("initiator"));
				trans.setTransTypeID(rs.getLong("transTypeID"));
				trans.setCreated(rs.getTimestamp("created"));
				trans.setDebtor(rs.getString("debtor"));
				trans.setCreditor(rs.getString("creditor"));
				trans.setRecipient(rs.getString("recipient"));
				trans.setProductID(rs.getString("productID"));
				trans.setAmount(rs.getString("amount"));
				trans.setParam1(rs.getString("param1"));
				trans.setParam2(rs.getString("param2"));
				trans.setParam3(rs.getString("param3"));
				trans.setParentID(rs.getLong("parentID"));
				trans.setState(rs.getString("state"));
				trans.setResultID(rs.getLong("resultID"));
				trans.setResultDesc(rs.getString("resultDesc"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return trans;
	}

	public Trans getTransBymerchantUniqueID(long merchantUniqueID, Transaction transaction,
			PropToTransaction propToTransaction) {
		Trans transRef = null;

		String strQuery = "select t.*,te.response from Transaction t,TransExtDetails te where te.transID=t.id and t.param1='"
				+ merchantUniqueID + "'";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transRef = new Trans();
				transRef.setId(rs.getLong("id"));
				transRef.setInitiator(rs.getString("initiator"));
				transRef.setTransTypeID(rs.getLong("transTypeID"));
				transRef.setCreated(rs.getDate("created"));
				transRef.setLastModified(rs.getDate("lastModified"));
				transRef.setDebtor(rs.getString("debtor"));
				transRef.setCreditor(rs.getString("creditor"));
				transRef.setAmount(rs.getString("amount"));
				transRef.setState(rs.getString("state"));
				transRef.setResultID(rs.getLong("resultID"));
				transRef.setParentID(rs.getLong("parentID"));
				transRef.setChannelID(rs.getLong("channelID"));
				transRef.setParam1(rs.getString("param1"));
				transRef.setParam2(rs.getString("param2"));
				transRef.setParam3(rs.getString("param3"));
				transRef.setRecipient(rs.getString("recipient"));
				transRef.setProductID(rs.getString("productID"));
				transRef.setIsAtomic(rs.getBoolean("isAtomic"));
				transRef.setResultDesc(rs.getString("response"));

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transRef;
	}

	public boolean isEmailRegistered(String msisdn, String email, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException {
		long entityId = 0;
		boolean isEmail = false;
		stm = null;
		TransactionDAOImpl transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		Connection conn = null;
		try {
			conn = DBPool.getConnection();
		} catch (Exception e) {

		}
		if (conn != null) {
			try {
				stm = conn.createStatement();
			} catch (SQLException e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
			}
			entityId = transactionDAO
					.getRowID("select ed.id from EntityData ed,Entity e where ed.entityID=e.id and ed.email='" + email
							+ "' and e.msisdn='" + msisdn + "'  and e.statusID='1' ");
		}
		if (entityId != 0) {
			isEmail = true;
		}
		return isEmail;
	}

	public boolean resetPassword(long entityId, String password, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException {
		boolean isUpdate = false;
		// Connection conn = new ConnectionPool().getConnection();
		try {
			conn = DBPool.getConnection();
			transactionDAO = new TransactionDAOImpl();
			isUpdate = transactionDAO.update(conn,
					"update password set password=?,lastModified=?,incorrectAttempts=? where entityID=?",
					new Object[] { password, new Date(), "0", entityId }, transaction, propToTransaction);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
							+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isUpdate;
	}

	public long createWallet(Register register, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException {
		long accountId = 0;
		long entityId = 0, entityData = 0;
		Statement stm = null;
		ResultSet rs = null;
		long typeID = 0, accountTypeId = 0;
		TransactionDAOImpl transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		Connection conn = null;
		try {
			conn = DBPool.getConnection();
		} catch (Exception e) {

		}
		if (conn != null) {
			try {
				stm = conn.createStatement();
			} catch (SQLException e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
			}
			typeID = transactionDAO.getRowID("select id from EntityType where type='wallet'");
			entityId = transactionDAO.save(conn, "insert into Entity(msisdn,name,statusID,typeID) values(?,?,?,?)",
					new Object[] { register.getMsisdn(), register.getName(), 1, typeID }, transaction,
					propToTransaction);
		}
		if (entityId != 0) {
			conn = DBPool.getConnection();
			entityData = transactionDAO.save(conn,
					"insert into EntityData(entityID,age,sex,address,email,identity_id,identity_no,regDate) values(?,?,?,?,?,?,?,?)",
					new Object[] { entityId, register.getDob(), register.getSex(), register.getAddress(),
							register.getEmail(), register.getIdentityTypeID(), register.getIdentityNo(), new Date() },
					transaction, propToTransaction);
			accountTypeId = transactionDAO.getRowID("select id from AccountType where type='mwallet'");
			conn = DBPool.getConnection();
			accountId = transactionDAO.save(conn,
					"insert into Accounts(entityID,lowerLimit,upperLimit,typeID) values(?,?,?,?)",
					new Object[] { entityId, 1, 20000, accountTypeId }, transaction, propToTransaction);
		}
		if (accountId == 0) {
			transactionDAO.deleteRowByID("delete from Entity where id=" + entityId);
			entityId = 0;
		}
		return entityId;
	}

	public Integer createOTP(Transaction transaction, PropToTransaction propToTransaction) throws SQLException {
		long id = 0;
		int otp = 0;
		TransactionDAOImpl transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		Connection conn = null;
		try {
			conn = DBPool.getConnection();
		} catch (Exception e) {

		}
		if (conn != null) {
			try {
				stm = conn.createStatement();
			} catch (SQLException e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
			}
			otp = Integer.parseInt(randomOTP(6));
			id = transactionDAO
					.save(conn, "insert into otp (otp,transID,created,expiry_date) values(?,?,?,?)",
							new Object[] { otp, transaction.getTransId(), new Date(),
									new Date(System.currentTimeMillis() + 10 * 60 * 1000) },
							transaction, propToTransaction);
			if (id == 0) {
				otp = 0;
			}
		}

		return otp;
	}

	public Integer checkOTP(Transaction transaction, PropToTransaction propToTransaction, Integer transID)
			throws SQLException {

		int otp = 0;
		try {
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery("select otp from otp where expiry_date > NOW() and transID=" + transID);
			while (rs.next()) {
				otp = Integer.parseInt(rs.getString("otp"));
			}
		} catch (Exception e) {
			otp = 0;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + "). ");
			}
		}

		return otp;
	}

	

	public boolean debitAccounts(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance-" + amount
					+ "),availableBalance=(availableBalance-" + amount + ") where entityID='" + entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug", transaction.getTransId() + " ~ D: " + strQuery.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}

	public boolean creditAccounts(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance+" + amount
					+ "),availableBalance=(availableBalance+" + amount + ") where entityID='" + entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug", transaction.getTransId() + " ~ D: " + strQuery.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}

	public long validateOTP(String trandId, String otp, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException {
		long otpId = 0;
		setStm(null);
		TransactionDAOImpl transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
		Connection conn = null;
		try {
			conn = DBPool.getConnection();
		} catch (Exception e) {

		}
		if (conn != null) {
			try {
				setStm(conn.createStatement());
			} catch (SQLException e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
			}
			otpId = transactionDAO.getRowID("select id from otp where transID='" + trandId + "' and otp='" + otp
					+ "' and expiry_date >= now()");
		}

		return otpId;
	}

	public String encryptPassword(String password, Transaction transaction, PropToTransaction propToTransaction) {
		String sha1 = "";
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(password.getBytes("UTF-8"));
			sha1 = byteToHex(crypt.digest());
		} catch (NoSuchAlgorithmException e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (UnsupportedEncodingException e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		}
		return sha1;
	}

	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	public long generatePassword(long entityId, String password, Transaction transaction,
			PropToTransaction propToTransaction) throws SQLException {
		long id = 0;
		try {
			Connection conn = DBPool.getConnection();
			TransactionDAOImpl transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);
			id = transactionDAO.save(conn,
					"insert into password(entityID,password,status,lastModified) values(?,?,?,?)",
					new Object[] { entityId, password, "1", new Date() }, transaction, propToTransaction);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
							+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + ").");
		}
		return id;
	}

	public String randomPassword(Integer len) {
		String AB = "123456789";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}

	public String randomOTP(Integer len) {
		String AB = "123456789";
		SecureRandom rnd = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}

	public String isValidDOB(String dob, int dobc, PropToTransaction propToTransaction) {
		String cdob = "0";
		String strDob[] = dob.split("-");
		int year = Integer.parseInt(strDob[0]);
		int month = Integer.parseInt(strDob[1]);
		int day = Integer.parseInt(strDob[2]);
		Date now = new Date();
		int nowMonth = now.getMonth() + 1;
		int nowYear = now.getYear() + 1900;
		int result = nowYear - year;

		if (month > nowMonth) {
			result--;
		} else if (month == nowMonth) {
			int nowDay = now.getDate();

			if (day > nowDay) {
				result--;
			}
		}

		if (result < dobc) {
			cdob = "0";
		} else {
			cdob = String.valueOf(result);
		}
		return cdob;
	}

	public String[] isActiveProductValid(String productID, Transaction trans, PropToTransaction propToTransaction) {
		String parameters[] = new String[3];
		// String strQuery = "select su.parameters,p.name from Product p,
		// ServiceURL su where su.productID=p.id and p.id='"
		// + productID + "' and p.isActive='1' limit 1";
		String strQuery = "select su.parameters,p.name,sp.name serviceProvider from Product p, ServiceURL su, ServiceProvider sp where su.productID=p.id and su.spID=sp.id and su.isActive=1 and p.isActive='1' and p.id='"
				+ productID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", trans.getTransId() + "~D~" + strQuery + " \n.initiator(" + trans.getInitiator()
							+ ")" + trans.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, trans, propToTransaction);
			while (rs.next()) {
				parameters[0] = rs.getString("parameters");
				parameters[1] = rs.getString("name");
				parameters[2] = rs.getString("serviceProvider");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ trans.getInitiator() + "). Performed by " + trans.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
		}
		return parameters;
	}

	@SuppressWarnings("unused")
	public List<GetActiveProduct> getProductDetail(String productNotIn, Transaction transaction,
			PropToTransaction propToTransaction) {
		List<GetActiveProduct> details = new ArrayList<GetActiveProduct>();
		String strQuery = null;
		if (productNotIn != null && productNotIn != "" && !productNotIn.equals("")) {
			strQuery = "select p.id,p.name,pt.name as productType,su.isFetchAvailable,su.amountRange,su.parameters,su.parameter_description,su.amountEditEnabled,su.parameter_regex from Product p,ProductType pt,ServiceURL su  where  p.productTypeID=pt.id and su.productID=p.id and p.isActive=1 and su.isActive=1 and p.id not in ("
					+ productNotIn + ") order by p.id asc";
		} else {
			strQuery = "select p.id,p.name,pt.name as productType,su.isFetchAvailable,su.amountRange,su.parameters,su.parameter_description,su.amountEditEnabled,su.parameter_regex from Product p,ProductType pt,ServiceURL su  where  p.productTypeID=pt.id and su.productID=p.id and p.isActive=1 and su.isActive=1 order by p.id asc";
		}
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				GetActiveProduct activeProduct = new GetActiveProduct();
				activeProduct.setProductId(rs.getInt("id"));
				activeProduct.setProductName(rs.getString("name"));
				activeProduct.setIsFeatchAvailable(rs.getInt("isFetchAvailable"));
				activeProduct.setAmountRange(rs.getString("amountRange"));
				activeProduct.setParameterRegex(rs.getString("parameter_regex"));
				activeProduct.setParameters(rs.getString("parameters"));
				activeProduct.setParameterDescription(rs.getString("parameter_description"));
				activeProduct.setAmountEditEnabled(rs.getString("amountEditEnabled"));
				activeProduct.setProductType(rs.getString("productType"));
				details.add(activeProduct);
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + CommonDAO.class), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + CommonDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + CommonDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + CommonDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}
			}
		}

		return details;
	}

	public GetTransactionStatus getTransDetails(String initiator, String merchantUniqueID, Transaction transaction,
			PropToTransaction propToTransaction) {
		GetTransactionStatus transdetail = null;
		String strQuery = "select t.state from Transaction t where t.initiator='" + initiator + "' and  t.param1='"
				+ merchantUniqueID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transdetail = new GetTransactionStatus();
				// transdetail.setType(rs.getString("type"));
				// transdetail.setStatus(rs.getString("status"));
				transdetail.setState(Integer.parseInt(rs.getString("state")));
				// transdetail.setResultCode(rs.getInt("resultCode"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transdetail;
	}

	public String[] getServiceProvider(String productID, Transaction trans, PropToTransaction propToTransaction) {
		String[] parameters = new String[3];
		/*
		 * Old Query
		 * 
		 * select su.parameters,p.name from Product p, ServiceURL su where
		 * su.productID=p.id and p.id='"+ productID + "' and p.isActive='1'
		 * limit 1
		 */

		String strQuery = "select su.parameters,p.name,sp.name serviceProvider from Product p, ServiceURL su, ServiceProvider sp where su.productID=p.id and su.spID=sp.id and su.isActive=1 and p.isActive='1' and sp.isActive='1' and p.id='"
				+ productID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", trans.getTransId() + "~D~" + strQuery + " \n.initiator(" + trans.getInitiator()
							+ ")" + trans.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, trans, propToTransaction);
			while (rs.next()) {
				parameters[0] = rs.getString("parameters");
				parameters[1] = rs.getString("name");
				parameters[2] = rs.getString("serviceProvider");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ trans.getInitiator() + "). Performed by " + trans.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
		}
		return parameters;
	}
	public String isActiveProduct(String productID, Transaction trans, PropToTransaction propToTransaction) {
		String parameters = null;
		// String strQuery = "select su.parameters,p.name from Product p,
		// ServiceURL su where su.productID=p.id and p.id='"
		// + productID + "' and p.isActive='1' limit 1";
		String strQuery = "select su.parameters,p.name,sp.name serviceProvider from Product p, ServiceURL su, ServiceProvider sp where su.productID=p.id and su.spID=sp.id and su.isActive=1 and p.isActive='1' and p.id='"
				+ productID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", trans.getTransId() + "~D~" + strQuery + " \n.initiator(" + trans.getInitiator()
							+ ")" + trans.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, trans, propToTransaction);
			while (rs.next()) {
				parameters = rs.getString("parameters");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ trans.getInitiator() + "). Performed by " + trans.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
		}
		return parameters;
	}
	public String getTransactionStatus(Trans trans, Transaction transaction, PropToTransaction propToTransaction) {
		String strState = null;
		String strQuery = "select t.state from Transaction t where t.id='" + trans.getId() + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				strState = rs.getString("state");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return strState;
	}

	public boolean updateTrans(Trans trans, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;
		// Connection conn = new ConnectionPool().getConnection();
		try {
			conn = DBPool.getConnection();
			transactionDAO = new TransactionDAOImpl();
			if (trans != null) {
				isUpdate = transactionDAO.update(conn,
						"update Transaction set transTypeID=?,lastModified=?,debtor=?,creditor=?,recipient=?,productID=?,amount=?,state=?,resultID=?,parentID=?,channelID=?,isAtomic=?,resultDesc=?,param1=?,param2=?,param3=?  where id=?",
						new Object[] { trans.getTransTypeID(), new Date(), trans.getDebtor(), trans.getCreditor(),
								trans.getRecipient(), trans.getProductID(), trans.getAmount(), trans.getState(),
								trans.getResultID(), trans.getParentID(), trans.getChannelID(), trans.getIsAtomic(),
								trans.getResultDesc(), trans.getParam1(), trans.getParam2(), trans.getParam3(),
								trans.getId() },
						transaction, propToTransaction);
			}

		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isUpdate;
	}

	public Commission getCommission(String productID, Transaction transaction, PropToTransaction propToTransaction) {
		Commission commission = null;
		String strQuery = "select c.* from Commission c where productID='" + productID + "'limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				commission = new Commission();
				commission.setTransTypeID(rs.getLong("transTypeID"));
				commission.setChannelID(rs.getLong("channelID"));
				commission.setGroupID(rs.getLong("groupID"));
				commission.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				commission.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				commission.setMethod(rs.getString("method"));
				commission.setFromAmount(rs.getString("fromAmount"));
				commission.setToAmount(rs.getString("toAmount"));
				commission.setCommAmount(rs.getString("commAmount"));
				commission.setCommPercentage(rs.getString("commPercentage"));
				commission.setCommtarget(rs.getString("commtarget"));
				commission.setCommSource(rs.getString("commSource"));

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return commission;
	}

	public boolean updateExtTransDetails(String response, Integer extResult, long transID, Transaction transaction,
			PropToTransaction propToTransaction) {
		boolean isUpdate = false;
		;
		try {
			String qry = "update TransExtDetails set response='" + response + "',extResult='" + extResult
					+ "',last_modified=now()  where transID='" + transID + "'";

			isUpdate = transactionDAO.updateById(qry);

		} catch (Exception ex) {

		}
		return isUpdate;
	}

	public Statement getStm() {
		return stm;
	}

	public void setStm(Statement stm) {
		this.stm = stm;
	}

	public static void main(String[] args) {
		System.out.println(new Date());

		System.out.println(new Date(System.currentTimeMillis() + 10 * 60 * 1000));

	}
}
