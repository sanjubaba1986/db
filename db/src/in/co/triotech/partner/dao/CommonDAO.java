package in.co.triotech.partner.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.billdesk.helper.ResponseResult;
import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.partner.GetTransactionStatus;
import in.co.triotech.triomoney.GetActiveProduct;
import in.co.triotech.triomoney.GetProductVersion;
import in.co.triotech.triomoney.dao.TransactionDAO;
import in.co.triotech.triomoney.dao.TransactionDAOImpl;
import in.co.triotech.triomoney.model.Commission;
import in.co.triotech.triomoney.model.TransExtDetails;
import in.co.triotech.triomoney.model.TransactionData;

public class CommonDAO {

	private TransactionDAO transactionDAO = null;

	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	DataSource dataSource = null;// ConnectionPool.setUp();
	Logger accessLog = Logger.getLogger("QL" + CommonDAO.class);
	Logger excepLog = Logger.getLogger("EL" + CommonDAO.class);

	public boolean updateTrans(Trans trans, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException, Exception {
		boolean isUpdate = false;
		// Connection conn = new ConnectionPool().getConnection();
		if(trans.getParam1().equals("0"))
		{
			trans.setParam1(null);
		}
		try {
			conn = DBPool.getConnection();
			transactionDAO = new TransactionDAOImpl();
			isUpdate = transactionDAO.update(conn,
					"update Transaction set transTypeID=?,lastModified=?,debtor=?,creditor=?,recipient=?,productID=?,amount=?,state=?,resultID=?,parentID=?,channelID=?,isAtomic=?,resultDesc=?,param1=?,param2=?,param3=?  where id=?",
					new Object[] { trans.getTransTypeID(), new Date(), trans.getDebtor(), trans.getCreditor(),
							trans.getRecipient(), trans.getProductID(), trans.getAmount(), trans.getState(),
							trans.getResultID(), trans.getParentID(), trans.getChannelID(), trans.getIsAtomic(),
							trans.getResultDesc(), trans.getParam1(), trans.getParam2(), trans.getParam3(),
							trans.getId() },
					transaction, propToTransaction);

		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return isUpdate;
	}

	public String getRandomAgentID(Transaction transaction, PropToTransaction propToTransaction) throws SQLException {

		String agent_id = null;
		try {
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = stm.executeQuery("SELECT agent_id from money_transfer_agent_info ORDER BY RAND() LIMIT 1");
			while (rs.next()) {
				agent_id = rs.getString("agent_id");
			}
		} catch (Exception e) {
			agent_id = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + "). ");
			}
		}

		return agent_id;
	}

	public String getBenId(String mobileNo, Transaction transaction, PropToTransaction propToTransaction)
			throws SQLException {

		String ben_id = null;
		try {
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = stm.executeQuery(
					"SELECT ben_id from money_transfer_customer_registration WHERE mobile='" + mobileNo + "' LIMIT 1");
			while (rs.next()) {
				ben_id = rs.getString("ben_id");
			}
		} catch (Exception e) {
			ben_id = null;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger "), "error", transaction.getTransId() + " ~ E: "
								+ errors.toString() + " \n.initiator(" + transaction.getInitiator() + "). ");
			}
		}

		return ben_id;
	}

	public boolean updateExtTransDetails(String response, Integer extResult, long transID, Transaction transaction,
			PropToTransaction propToTransaction) {
		boolean isUpdate = false;
		;
		try {
			String qry = "update TransExtDetails set response='" + response + "',extResult='" + extResult
					+ "',last_modified=now()  where transID='" + transID + "'";

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + qry + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());

			isUpdate = transactionDAO.updateById(qry);

		} catch (Exception ex) {

		}
		return isUpdate;
	}

	public void reverseFee(long transId, Trans trans, Entity entity, Transaction transaction,
			PropToTransaction propToTransaction) {
		try {
			String strQuery = "SELECT id,amount,creditor,debtor from Transaction where parentID=" + transId
					+ " and transTypeID=35 and state=2";
			transactionDAO = new TransactionDAOImpl();
			try {
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "debug", transaction.getTransId() + " ~ " + strQuery.toString() + ". initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());
				conn = DBPool.getConnection();
				stm = conn.createStatement();
				rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
				while (rs.next()) {
					CommonDAO commonDAO = new CommonDAO();
					commonDAO.creditAccounts(rs.getLong("debtor"), rs.getDouble("amount"), transId, transaction,
							propToTransaction);
					commonDAO.debitAccounts(rs.getLong("creditor"), rs.getDouble("amount"), transId, transaction,
							propToTransaction);
					transactionDAO.updateById(
							"UPDATE Transaction set state=5,resultDesc='Parent transaction failed.' WHERE id="
									+ rs.getLong("id"));
					ReadLog4J.writeLog(
							"Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							accessLog, "info", transaction.getTransId() + ", Fee Amount: +" + rs.getDouble("amount")
									+ " Fee rollback sucessfully from " + rs.getLong("creditor"));
				}
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog(
						"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						Logger.getLogger("Exception logger " + CommonDAO.class), "error",
						transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());

			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (Exception e) {
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger " + CommonDAO.class), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());

					}
				}
				if (stm != null) {
					try {
						stm.close();
					} catch (Exception e) {
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger " + CommonDAO.class), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());

					}
				}
				if (conn != null) {
					try {
						conn.close();
					} catch (Exception e) {
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger " + CommonDAO.class), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());
					}
				}
			}

		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + CommonDAO.class), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		}
	}

	public boolean debitAccounts(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance-" + amount
					+ "),availableBalance=(availableBalance-" + amount + ") where entityID='" + entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug", transaction.getTransId() + " ~ D: " + strQuery.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}

	public boolean creditAccounts(long entityID, double amount, long transid, Transaction transaction,
			PropToTransaction propToTransaction) {

		boolean isUpdate = false;
		try {
			transactionDAO = new TransactionDAOImpl(transaction, propToTransaction);

			String strQuery = "update Accounts set currentBalance=(currentBalance+" + amount
					+ "),availableBalance=(availableBalance+" + amount + ") where entityID='" + entityID + "'";
			isUpdate = transactionDAO.updateById(strQuery);
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "debug", transaction.getTransId() + " ~ D: " + strQuery.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
		} catch (SQLException e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());

		} catch (Exception e) {
			StackTraceElement l = e.getStackTrace()[0];
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					transaction.getTransId() + "~E~" + errors.toString() + l.getClassName() + "/" + l.getMethodName()
							+ ":" + l.getLineNumber() + " \n.initiator(" + transaction.getInitiator() + ")"
							+ transaction.getThreadName());
		}

		return isUpdate;
	}

	public boolean updateExtTransDetails(String response, Integer extResult, long transID, String responseDesc,
			Transaction transaction, PropToTransaction propToTransaction) {
		boolean isUpdate = false;
		;
		try {
			String qry = "update TransExtDetails set response='" + response + "',extResult='" + extResult
					+ "',extResultDescription='" + responseDesc + "',last_modified=now()  where transID='" + transID
					+ "'";

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + qry + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());

			isUpdate = transactionDAO.updateById(qry);

		} catch (Exception ex) {

		}
		return isUpdate;
	}
	// param1

	public GetTransactionStatus getTransDetails(String initiator, String merchantUniqueID, Transaction transaction,
			PropToTransaction propToTransaction) {
		GetTransactionStatus transdetail = null;
		String strQuery = "select t.* from Transaction t where t.initiator='" + initiator + "' and  ( t.param1='"
				+ merchantUniqueID + "')  limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transdetail = new GetTransactionStatus();
				// transdetail.setType(rs.getString("type"));
				// transdetail.setStatus(rs.getString("status"));
				transdetail.setState(Integer.parseInt(rs.getString("state")));
				transdetail.setResultCode(Integer.parseInt(String.valueOf(rs.getLong("resultID"))));
				transdetail.setParentTransID(rs.getLong("id"));
				transdetail.setResultDesc(rs.getString("resultDesc"));
				// transdetail.setResultCode(rs.getInt("resultCode"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
					ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transdetail;
	}
	public ResponseResult getTransDetails(Entity entity, ResponseResult responseResult, Transaction transaction,
			PropToTransaction propToTransaction) {
		ResponseResult transdetail = null;
		String strQuery = "";
		if (entity.getTypeID() == 4) {
			strQuery = "select t.* from Transaction t where t.param1='" + responseResult.getMerchantUniqueID() + "' or t.id='"
					+  responseResult.getMerchantUniqueID() + "' limit 1";
		} else {
			strQuery  = "select t.* from Transaction t where t.initiator='" + transaction.getInitiator() + "' and  ( t.param1='"
					+ responseResult.getMerchantUniqueID() + "' or t.id='" + responseResult.getMerchantUniqueID() + "')  limit 1";
		}
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transdetail = new ResponseResult();
				transdetail.setState(Integer.parseInt(rs.getString("state")));
				transdetail.setResultCode(Integer.parseInt(String.valueOf(rs.getLong("resultID"))));
				transdetail.setParentTransID(rs.getLong("id"));
				transdetail.setResultDesc(rs.getString("resultDesc"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transdetail;
	}

	public TransactionData getTransDataByID(long transID, Transaction transaction,
			PropToTransaction propToTransaction) {
		TransactionData transactionData = null;
		String strQuery = "select * from Transaction_Data WHERE transID=" + transID;
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transactionData = new TransactionData();
				transactionData.setTransID(rs.getLong("transID"));
				transactionData.setKeyType(rs.getString("keyType"));
				transactionData.setKeyVal(rs.getString("keyVal"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transactionData;
	}

	public Trans getTransBymerchantUniqueID(long merchantUniqueID, Transaction transaction,
			PropToTransaction propToTransaction) {
		Trans transRef = null;

		String strQuery = "select t.*,te.request,te.response from Transaction t,TransExtDetails te where te.transID=t.id and t.param1='"
				+ merchantUniqueID + "'";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transRef = new Trans();
				transRef.setId(rs.getLong("id"));
				transRef.setInitiator(rs.getString("initiator"));
				transRef.setTransTypeID(rs.getLong("transTypeID"));
				transRef.setCreated(rs.getDate("created"));
				transRef.setLastModified(rs.getDate("lastModified"));
				transRef.setDebtor(rs.getString("debtor"));
				transRef.setCreditor(rs.getString("creditor"));
				transRef.setAmount(rs.getString("amount"));
				transRef.setState(rs.getString("state"));
				transRef.setResultID(rs.getLong("resultID"));
				transRef.setParentID(rs.getLong("parentID"));
				transRef.setChannelID(rs.getLong("channelID"));
				transRef.setParam1(rs.getString("param1"));
				transRef.setParam2(rs.getString("param2"));
				transRef.setParam3(rs.getString("request"));
				transRef.setRecipient(rs.getString("recipient"));
				transRef.setProductID(rs.getString("productID"));
				transRef.setIsAtomic(rs.getBoolean("isAtomic"));
				transRef.setResultDesc(rs.getString("response"));

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transRef;
	}

	public GetTransactionStatus getTransDetails(String merchantUniqueID, Transaction transaction,
			PropToTransaction propToTransaction) {
		GetTransactionStatus transdetail = null;
		String strQuery = "select t.* from Transaction t where t.param1='" + merchantUniqueID + "' or t.id='"
				+ merchantUniqueID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transdetail = new GetTransactionStatus();
				// transdetail.setType(rs.getString("type"));
				// transdetail.setStatus(rs.getString("status"));
				transdetail.setParentTransID(rs.getLong("id"));
				transdetail.setState(Integer.parseInt(rs.getString("state")));
				transdetail.setResultCode(Integer.parseInt(String.valueOf(rs.getLong("resultID"))));
				transdetail.setResultDesc(rs.getString("resultDesc"));
				// transdetail.setResultCode(rs.getInt("resultCode"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transdetail;
	}

	public Trans getTrans(Trans trans, Transaction transaction, PropToTransaction propToTransaction) {
		Trans transToVerify = null;
		String strQuery = "select t.* from Transaction t where t.id='" + trans.getId() + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				transToVerify = new Trans();
				transToVerify.setId(rs.getLong("id"));
				transToVerify.setInitiator(rs.getString("initiator"));
				transToVerify.setTransTypeID(rs.getLong("transTypeID"));
				transToVerify.setDebtor(rs.getString("debtor"));
				transToVerify.setCreditor(rs.getString("creditor"));
				transToVerify.setRecipient(rs.getString("recipient"));
				transToVerify.setProductID(rs.getString("productID"));
				transToVerify.setAmount(rs.getString("amount"));
				transToVerify.setState(rs.getString("state"));
				transToVerify.setResultID(rs.getLong("resultID"));
				transToVerify.setParentID(rs.getLong("parentID"));
				transToVerify.setChannelID(rs.getLong("channelID"));
				transToVerify.setParam1(rs.getString("param1"));
				transToVerify.setParam2(rs.getString("param2"));
				transToVerify.setParam3(rs.getString("param3"));
				transToVerify.setIsAtomic(rs.getBoolean("isAtomic"));
				transToVerify.setResultDesc(rs.getString("resultDesc"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transToVerify;
	}

	public String getTransactionStatus(Trans trans, Transaction transaction, PropToTransaction propToTransaction) {
		String strState = null;
		String strQuery = "select t.state from Transaction t where t.id='" + trans.getId() + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				strState = rs.getString("state");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return strState;
	}

	public Commission getCommission(String productID, Transaction transaction, PropToTransaction propToTransaction) {
		Commission commission = null;
		String strQuery = "select c.* from Commission c where productID='" + productID + "'limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				commission = new Commission();
				commission.setTransTypeID(rs.getLong("transTypeID"));
				commission.setChannelID(rs.getLong("channelID"));
				commission.setGroupID(rs.getLong("groupID"));
				commission.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				commission.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				commission.setMethod(rs.getString("method"));
				commission.setFromAmount(rs.getString("fromAmount"));
				commission.setToAmount(rs.getString("toAmount"));
				commission.setCommAmount(rs.getString("commAmount"));
				commission.setCommPercentage(rs.getString("commPercentage"));
				commission.setCommtarget(rs.getString("commtarget"));
				commission.setCommSource(rs.getString("commSource"));

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return commission;
	}

	public Commission getCommissionByType(long transTypeId, Transaction transaction,
			PropToTransaction propToTransaction) {
		Commission commission = null;
		String strQuery = "select c.* from Commission c where transTypeID='" + transTypeId + "'limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				commission = new Commission();
				commission.setTransTypeID(rs.getLong("transTypeID"));
				commission.setChannelID(rs.getLong("channelID"));
				commission.setGroupID(rs.getLong("groupID"));
				commission.setTransAccountTypeID(rs.getLong("transAccountTypeID"));
				commission.setTargetAccountTypeID(rs.getLong("targetAccountTypeID"));
				commission.setMethod(rs.getString("method"));
				commission.setFromAmount(rs.getString("fromAmount"));
				commission.setToAmount(rs.getString("toAmount"));
				commission.setCommAmount(rs.getString("commAmount"));
				commission.setCommPercentage(rs.getString("commPercentage"));
				commission.setCommtarget(rs.getString("commtarget"));
				commission.setCommSource(rs.getString("commSource"));

			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return commission;
	}

	public boolean isActiveProductType(String productType, Transaction trans, PropToTransaction propToTransaction) {
		boolean isActive = false;
		String strQuery = "select * from ProductType where name='" + productType + "' and isActive='1'  limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", trans.getTransId() + "~D~" + strQuery + " \n.initiator(" + trans.getInitiator()
							+ ")" + trans.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, trans, propToTransaction);
			while (rs.next()) {
				isActive = true;
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ trans.getInitiator() + "). Performed by " + trans.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
		}
		return isActive;
	}

	public String isActiveProductCellmoneyCyberPlat(String productID, Transaction trans,
			PropToTransaction propToTransaction) {
		String parameters = null;
		/*
		 * Old Query
		 * 
		 * select su.parameters,p.name from Product p, ServiceURL su where
		 * su.productID=p.id and p.id='"+ productID + "' and p.isActive='1'
		 * limit 1
		 */

		String strQuery = "select su.parameters,p.name,sp.name serviceProvider from Product p, ServiceURL su, ServiceProvider sp where su.productID=p.id and su.spID=sp.id and su.isActive=1 and p.isActive='1' and p.id='"
				+ productID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", trans.getTransId() + "~D~" + strQuery + " \n.initiator(" + trans.getInitiator()
							+ ")" + trans.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, trans, propToTransaction);
			while (rs.next()) {
				parameters = rs.getString("parameters");
				parameters += "~" + rs.getString("serviceProvider");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ trans.getInitiator() + "). Performed by " + trans.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
		}
		return parameters;
	}

	public String isActiveProduct(String productID, Transaction trans, PropToTransaction propToTransaction) {
		String parameters = null;
		// String strQuery = "select su.parameters,p.name from Product p,
		// ServiceURL su where su.productID=p.id and p.id='"
		// + productID + "' and p.isActive='1' limit 1";
		String strQuery = "select su.parameters,p.name,sp.name serviceProvider from Product p, ServiceURL su, ServiceProvider sp where su.productID=p.id and su.spID=sp.id and su.isActive=1 and p.isActive='1' and p.id='"
				+ productID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", trans.getTransId() + "~D~" + strQuery + " \n.initiator(" + trans.getInitiator()
							+ ")" + trans.getThreadName());
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, trans, propToTransaction);
			while (rs.next()) {
				parameters = rs.getString("parameters");
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("EL"), "error",
					"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ trans.getInitiator() + "). Performed by " + trans.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("EL"), "error",
							"Transaction Id: " + trans.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + trans.getInitiator() + "). Performed by "
									+ trans.getThreadName());
				}
			}
		}
		return parameters;
	}

	@SuppressWarnings("null")
	public GetProductVersion getProductVersion(PropToTransaction propToTransaction, Transaction trans) {
		ResultSet rs = null;
		Statement stm = null;
		Connection con = null;
		GetProductVersion productVersion = null;
		int id = 0;
		try {
			// dataSource = ConnectionPool.setUp(trans,propToTransaction);
			con = DBPool.getConnection();// dataSource.getConnection();
			stm = con.createStatement();
			rs = stm.executeQuery("select version from ProductVersion where isActive='1'  order by id desc limit 1 ");
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug",
					trans.getTransId() + "~D~"
							+ "select version from ProductVersion where isActive='1'  order by id desc limit 1 "
							+ " \n.initiator(" + trans.getInitiator() + ")" + trans.getThreadName());
			while (rs.next()) {
				productVersion = new GetProductVersion();
				productVersion.setVersion(rs.getString("version"));
			}
		} catch (Exception e) {
			id = 0;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stm != null)
					stm.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
			}
		}

		return productVersion;
	}

	public List<GetActiveProduct> getProductDetail(Transaction transaction, PropToTransaction propToTransaction) {
		List<GetActiveProduct> details = new ArrayList<GetActiveProduct>();
		String strQuery = "select p.id,p.name,pt.name as productType,su.isFetchAvailable,su.amountRange,su.parameters,su.parameter_description,su.amountEditEnabled,su.parameter_regex from Product p,ProductType pt,ServiceURL su  where  p.productTypeID=pt.id and su.productID=p.id and p.isActive=1 and su.isActive=1 order by p.id asc";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + " ~ " + strQuery.toString() + ". initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				GetActiveProduct activeProduct = new GetActiveProduct();

				activeProduct.setProductId(rs.getInt("id"));
				activeProduct.setProductName(rs.getString("name"));
				activeProduct.setIsFeatchAvailable(rs.getInt("isFetchAvailable"));
				activeProduct.setAmountRange(rs.getString("amountRange"));
				activeProduct.setParameterRegex(rs.getString("parameter_regex"));
				activeProduct.setParameters(rs.getString("parameters"));
				activeProduct.setParameterDescription(rs.getString("parameter_description"));
				activeProduct.setAmountEditEnabled(rs.getString("amountEditEnabled"));
				activeProduct.setProductType(rs.getString("productType"));
				details.add(activeProduct);
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger " + CommonDAO.class), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + CommonDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + CommonDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger " + CommonDAO.class), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}
			}
		}

		return details;
	}

	public Trans getTransByTransID(long transID, Transaction transaction, PropToTransaction propToTransaction) {
		Trans trans = null;
		String strQuery = "select t.* from Transaction t where t.id='" + transID + "'  limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {
				trans = new Trans();
				trans.setId(rs.getLong("id"));
				trans.setInitiator(rs.getString("initiator"));
				trans.setTransTypeID(rs.getLong("transTypeID"));
				trans.setCreated(rs.getTimestamp("created"));
				trans.setDebtor(rs.getString("debtor"));
				trans.setCreditor(rs.getString("creditor"));
				trans.setRecipient(rs.getString("recipient"));
				trans.setProductID(rs.getString("productID"));
				trans.setAmount(rs.getString("amount"));
				trans.setParam1(rs.getString("param1"));
				trans.setParam2(rs.getString("param2"));
				trans.setParam3(rs.getString("param3"));
				trans.setParentID(rs.getLong("parentID"));
				trans.setState(rs.getString("state"));
				trans.setResultID(rs.getLong("resultID"));
				trans.setResultDesc(rs.getString("resultDesc"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return trans;
	}

	public boolean updateExtTransDetails(String response, Integer extResult, String extResultDes, long transID,
			Transaction transaction, PropToTransaction propToTransaction) {
		boolean isUpdate = false;
		;
		try {
			String qry = "update TransExtDetails set response='" + response + "',extResult='" + extResult
					+ "',extResultDescription='" + extResultDes + "',last_modified=now()  where transID='" + transID
					+ "'";

			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + qry + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());

			isUpdate = transactionDAO.updateById(qry);

		} catch (Exception ex) {

		}
		return isUpdate;
	}

	public TransExtDetails getTransExtDetailsByTransID(long transID, Transaction transaction,
			PropToTransaction propToTransaction) {
		TransExtDetails transExtDetails = null;
		String strQuery = "select * from TransExtDetails where transID='" + transID + "' limit 1";
		transactionDAO = new TransactionDAOImpl();
		try {
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "debug", transaction.getTransId() + "~D~" + strQuery + " \n.initiator("
							+ transaction.getInitiator() + ")" + transaction.getThreadName());
			conn = DBPool.getConnection();// dataSource.getConnection();
			stm = conn.createStatement();
			rs = transactionDAO.executeQuery(strQuery, stm, transaction, propToTransaction);
			while (rs.next()) {

				transExtDetails = new TransExtDetails();
				transExtDetails.setId(rs.getLong("id"));
				transExtDetails.setTransID(rs.getLong("transID"));
				transExtDetails.setCreated(rs.getTimestamp("created"));
				transExtDetails.setLastModified(rs.getTimestamp("last_modified"));
				transExtDetails.setRequest(rs.getString("request"));
				transExtDetails.setResponse(rs.getString("response"));
				transExtDetails.setExtResult(String.valueOf(rs.getInt("extResult")));
				transExtDetails.setExtResultDescription(rs.getString("extResultDescription"));
			}
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
					excepLog, "error",
					"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + "). Performed by " + transaction.getThreadName());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + GetTimeStamp.getDate() + ".log",
							excepLog, "error",
							"Transaction Id: " + transaction.getTransId() + " ~ Error: " + errors.toString()
									+ " \n.initiator(" + transaction.getInitiator() + "). Performed by "
									+ transaction.getThreadName());
				}
			}
		}
		return transExtDetails;
	}

}
