package in.co.triotech.partner.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.dao.TransactionDAO;
import in.co.triotech.triomoney.dao.TransactionDAOImpl;

public class CustomerRegistrationDAO {
	private TransactionDAO transactionDAO = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	private DataSource dataSource = null;// ConnectionPool.setUp();

	public long createCustomer(String mobile, String ben_id, String aid, Transaction transaction,
			PropToTransaction propToTransaction) {
		long isCreate = 0;
		try {
			// dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();// dataSource.getConnection();
			transactionDAO = new TransactionDAOImpl();

			isCreate = transactionDAO.save(conn,
					"INSERT into money_transfer_customer_registration(mobile,ben_id,aid) values(?,?,?)",
					new Object[] { mobile, ben_id, aid }, transaction, propToTransaction);
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger "), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}
			}
		}

		return isCreate;
	}

}
