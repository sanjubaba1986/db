package in.co.triotech.partner.dao;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Formatter;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import in.co.triotech.connectivity.PropertyToMap;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.Accounts;
import in.co.triotech.model.Entity;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Trans;
import in.co.triotech.model.Transaction;
import in.co.triotech.partner.LoginAggregator;
import in.co.triotech.queue.Worker;
import in.co.triotech.triomoney.dao.BasicTransCheck;
import in.co.triotech.triomoney.dao.EntityDAOService;
import in.co.triotech.triomoney.dao.SystemConfigDAOService;
import in.co.triotech.triomoney.dao.TransactionDAO;
import in.co.triotech.triomoney.dao.TransactionDaoService;
import in.co.triotech.triomoney.model.Password;


public class LoginDAO {
	TransactionDAO transactionDAO = null;
	PropertyToMap propertyToMap = new PropertyToMap();
	// SystemConfigDAOService configDAOService=null;
	Password password = null;
	PasswordDAOService passwordDAOService = null;
	Logger accessLog = Logger.getLogger("WL");
	private JSONObject jsonObj = null;

	public int validateLogin(LoginAggregator validate, BlockingQueue<Transaction> senderQueue, Trans trans, Transaction transaction,
			Entity newEntity, Accounts accounts, PropToTransaction propToTransaction) throws SQLException, Exception {
		int isCorrect = 0;
		BasicTransCheck basicTransCheck = new BasicTransCheck();
		SystemConfigDAOService configDAOService = new SystemConfigDAOService();
		TransactionDaoService transactionDaoService = new TransactionDaoService();
		String incorrectAttempts = configDAOService.getSystemConfigFromList(propToTransaction.getSystemConfigs(),
				"incorrectAttempts");

		if (basicTransCheck.checkIncorrectAttempts(transaction, incorrectAttempts, validate.getPassword().toString(),
				propToTransaction)) {

			jsonObj = new JSONObject();
			jsonObj.put("Status", "error");
			jsonObj.put("Result Code", 1000);
			jsonObj.put("Result Namespace", "triomoney");
			jsonObj.put("TID", trans.getId());

			if (transaction.getOut() != null) {
				try {
					transaction.getOut().writeObject(jsonObj.toString());
					transaction.getOut().flush();
					transaction.getOut().close();
					transaction.setOut(null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger "), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}

			}
			EntityDAOService entityDAOService = new EntityDAOService();
			entityDAOService.setLastLogin(newEntity.getId(), transaction, propToTransaction);
			isCorrect = 1;
			trans.setState("3");
			trans.setResultDesc("account has been locked due to repeated failed login attempts," + newEntity.getName()
					+ "," + newEntity.getId());
			ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					accessLog, "info",
					transaction.getTransId() + " ~I:  account has been locked due to repeated failed login attempts("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
			transactionDaoService.incorrectAttempts(newEntity.getId().toString(), transaction, propToTransaction);
			transactionDaoService.updateCashinTrans(trans, transaction, propToTransaction);
			transaction = null;
		} else {
			if (basicTransCheck.isMPincodeCorrect(transaction, validate.getPassword().toString(), propToTransaction)) {

				jsonObj = new JSONObject();
				jsonObj.put("Status", "success");
				jsonObj.put("Result Code", 0);
				jsonObj.put("details",
						newEntity.getName() + "|" + transaction.getInitiator() + "|0|"
								+ newEntity.getLast_login() + "|" + newEntity.getId() + "|" + newEntity.getTypeID());
				jsonObj.put("TID", trans.getId());

				if (transaction.getOut() != null) {
					try {
						transaction.getOut().writeObject(jsonObj.toString());
						transaction.getOut().flush();
						transaction.getOut().close();
						transaction.setOut(null);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger "), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());
					}

				}

				EntityDAOService entityDAOService = new EntityDAOService();
				entityDAOService.setLastLogin(newEntity.getId(), transaction, propToTransaction);
				newEntity = entityDAOService.getEntityByEntityID(newEntity.getId(), transaction, propToTransaction);
				transactionDaoService.updateIncorrectAttempts(Long.parseLong(newEntity.getId().toString()), transaction,
						propToTransaction);
				isCorrect = 1;
				trans.setState("2");
				trans.setResultDesc("Success");
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~I: M pin is correct. initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());
				transactionDaoService.updateCashinTrans(trans, transaction, propToTransaction);
			} else {
				jsonObj = new JSONObject();
				jsonObj.put("Status", "error");
				jsonObj.put("Result Code", 535);
				jsonObj.put("Result Namespace", "triomoney");
				jsonObj.put("TID", trans.getId());

				if (transaction.getOut() != null) {
					try {
						transaction.getOut().writeObject(jsonObj.toString());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger "), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());
					}
					try {
						transaction.getOut().flush();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						StringWriter errors = new StringWriter();
						e1.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger "), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());
					}
					try {
						transaction.getOut().close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger "), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());
					}
				} else {
					try {
						transaction.getOut().close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						ReadLog4J.writeLog(
								"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
								Logger.getLogger("Exception logger "), "error",
								transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
										+ transaction.getInitiator() + ")." + transaction.getThreadName());
					}
				}
				ReadLog4J.writeLog("Worker_Logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
						accessLog, "info", transaction.getTransId() + " ~I: M pin is in-correct. initiator("
								+ transaction.getInitiator() + ")." + transaction.getThreadName());
				// ReadLog4J.writeLogger(GetTimeStamp.getDateTime(), "INFO",
				// "workerlogger", "M pin is in-correct
				// "+transaction.getInitiator()
				// + transaction.getData() + " performed by "
				// +
				// Thread.currentThread().getName(),LogConfiguration.writeLogPath()+"Worker_Logs/"+transaction.getTo()+"/"+GetTimeStamp.getDate()+".log");

				try {
					trans.setState("3");
					trans.setResultDesc(prepareMSG(propToTransaction, String.valueOf(trans.getId()),
							"msg.initiator.535", "M-Pin is not correct"));
					transactionDaoService.incorrectAttempts(newEntity.getId().toString(), transaction,
							propToTransaction);
					transactionDaoService.updateCashinTrans(trans, transaction, propToTransaction);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				String createCommand = transaction.getInitiator() + "#" + prepareMSG(propToTransaction,
						String.valueOf(trans.getId()), "msg.initiator.535", "M-Pin is not correct");
				transaction = propertyToMap.setTransaction("login", transaction.getFrom(), transaction.getTo(),
						transaction.getInitiator(), createCommand, transaction.getSourceip(),
						transaction.getSourceport(), transaction.getChannelId());
				try {
					new Worker().putTransactionToSender(senderQueue, transaction);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					ReadLog4J.writeLog(
							"Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
							Logger.getLogger("Exception logger "), "error",
							transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
									+ transaction.getInitiator() + ")." + transaction.getThreadName());
				}

				transaction = null;
			}
		}
		return isCorrect;
	}

	public String prepareMSG(PropToTransaction propToTransaction, String transID, String key, String defMessage) {
		String strMSG = propertyToMap.getPropertyValue(propToTransaction.getPropToMap(), key);
		if (strMSG != null) {
			strMSG = strMSG + " Transaction ID: " + transID;
		} else {
			strMSG = defMessage + ",Transaction ID: " + transID;
		}

		return strMSG;
	}

	@SuppressWarnings("unused")
	private static String encryptPassword(String password, Transaction transaction,
			PropToTransaction propToTransaction) {
		String sha1 = "";
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.reset();
			crypt.update(password.getBytes("UTF-8"));
			sha1 = byteToHex(crypt.digest());
		} catch (NoSuchAlgorithmException e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		} catch (UnsupportedEncodingException e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ReadLog4J.writeLog("Exception_logs/" + propToTransaction.getTo() + "/" + GetTimeStamp.getDate() + ".log",
					Logger.getLogger("Exception logger "), "error",
					transaction.getTransId() + " ~ E: " + errors.toString() + " \n.initiator("
							+ transaction.getInitiator() + ")." + transaction.getThreadName());
		}
		return sha1;
	}

	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}
}
