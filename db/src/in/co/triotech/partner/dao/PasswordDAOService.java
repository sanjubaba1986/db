package in.co.triotech.partner.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import in.co.triotech.db.DBPool;
import in.co.triotech.logs.ReadLog4J;
import in.co.triotech.model.GetTimeStamp;
import in.co.triotech.model.PropToTransaction;
import in.co.triotech.model.Transaction;
import in.co.triotech.triomoney.dao.TransactionDAO;
import in.co.triotech.triomoney.dao.TransactionDAOImpl;
import in.co.triotech.triomoney.model.Password;

public class PasswordDAOService {
	private TransactionDAO  transactionDAO=null;
	private Password password = null;
	private Connection conn;
	private Statement stm;
	private ResultSet rs;
	private DataSource dataSource =null;// ConnectionPool.setUp();
	
	
	public Password getPasswordByEntityID(Long entityID,Transaction transaction,PropToTransaction propToTransaction)
	{
		String strQuery = "select p.* from password p where p.entityID='"+entityID+"'";
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,transaction,propToTransaction);
		while(rs.next())
		{
			password = new Password(rs.getLong("id"), rs.getLong("entityID"), rs.getString("password"),rs.getDate("lastModified"), rs.getString("status"), rs.getString("incorrectAttempts"), rs.getString("old1"), rs.getString("old2"), rs.getString("old3"), rs.getString("old4"), rs.getString("old5"));
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
				}
			}
		}
		
		return password;
	}
	public boolean updatePassword(Password password,Transaction transaction,PropToTransaction propToTransaction)
	{
		boolean isUpdate = false;
		try{
			//dataSource = ConnectionPool.setUp(transaction,propToTransaction);
		 conn = DBPool.getConnection();//dataSource.getConnection();
		transactionDAO = new TransactionDAOImpl();

		isUpdate = transactionDAO.update(conn, "update password set password=?,lastModified=?,old1=?,old2=?,old3=?,old4=?,old5=?,incorrectAttempts=? where entityID=?", new Object[]{password.getPassword(),new Date(),password.getOld1(),password.getOld2(),password.getOld3(),password.getOld4(),password.getOld5(),"0",password.getEntityID()},transaction,propToTransaction);
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
		}
		finally{
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+"/"+GetTimeStamp.getDate()+".log",  Logger.getLogger("Exception logger "),"error",transaction.getTransId()+" ~ E: "+errors.toString()+" \n.initiator("+transaction.getInitiator()+")."+transaction.getThreadName());
				}
			}
		}
		
		return isUpdate;
	}
	
	
	public Boolean checkAllPassword(String confPin,long entityID,Transaction trans,PropToTransaction propToTransaction) {
		boolean isCorrect = false;
		transactionDAO = new TransactionDAOImpl(trans,propToTransaction);
		String query = "select id from password  where entityID='"+entityID+"' and (password=SHA1('"+confPin+"') or old1=SHA1('"+confPin+"') or old2=SHA1('"+confPin+"') or old3=SHA1('"+confPin+"') or old4=SHA1('"+confPin+"') or old5=SHA1('"+confPin+"'))";
		boolean isValidTrans = transactionDAO.isValid(query);
		if (isValidTrans) {
			isCorrect = true;
		}
	return isCorrect;
	}
	public long checkregisteredCustomer(String msisdn,Transaction trans,PropToTransaction propToTransaction) {
		long entityId=0;
		String strQuery = "select e.id from Entity e ,password p where p.entityID=e.id and e.msisdn='"+msisdn+"' limit 1" ;
		transactionDAO = new TransactionDAOImpl();
		try{
			//dataSource = ConnectionPool.setUp(trans,propToTransaction);
			conn = DBPool.getConnection();//dataSource.getConnection();
			stm = conn.createStatement();
			 rs = transactionDAO.executeQuery(strQuery,stm,trans,propToTransaction);
		while(rs.next())
		{
			entityId= rs.getInt("id");
		}
		}
		catch(Exception e){
			StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
		}
		finally{
			if(rs!=null){
				try{
					rs.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(stm!=null){
				try{
					stm.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
			if(conn!=null){
				try{
					conn.close();
				}
				catch(Exception e){
					StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				ReadLog4J.writeLog("Exception_logs/"+propToTransaction.getTo()+GetTimeStamp.getDate()+".log",  Logger.getLogger("EL"),"error","Transaction Id: "+trans.getTransId()+" ~ Error: "+errors.toString()+" \n.initiator("+trans.getInitiator()+"). Performed by "
						+ trans.getThreadName());
				}
			}
		}
			return entityId;
   }
}
